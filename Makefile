PYTHON:=python
MYPY_OPTS:=--strict
PEP8_OPTS:=--max-line-length=100 \
	--ignore=E251 \
	--show-source

all: check test install

check:
	${PYTHON} -m flake8 ${PEP8_OPTS} grimoire
	${PYTHON} -m mypy ${MYPY_OPTS} grimoire

test:
	${PYTHON} -m unittest

install:
	${PYTHON} -m pip install -e .

uninstall:
	${PYTHON} -m pip uninstall -y grimoire

clean:
	find . -type f -name "*.py[co]" -delete -o -type d -name __pycache__ -delete

.PHONY: check test install uninstall clean
