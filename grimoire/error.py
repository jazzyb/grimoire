class GrimoireError(RuntimeError):
    @property
    def message(self) -> str:
        return str(self.args[0])
