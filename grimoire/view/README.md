# Grimoire Visual Interface

When `grimoire` is first started, you'll see a set of panels in the following
configuration:

```
+---------------------------------------+
|Search:                                |
+--------++-------------++--------------+
|        ||             ||              |
|  (A)   ||     (C)     ||     (D)      |
|        ||             ||              |
+--------+|             ||              |
+--------+|             ||              |
|        ||             ||              |
|  (B)   ||             ||              |
|        ||             ||              |
+--------++-------------++--------------+
|run:               |                   |
+---------------------------------------+
```

The default panels are:
* Top:  [the Search Bar](#the-search-bar)
* (A):  [the Card Panel](#a-the-card-panel)
* (B):  [the Scope Panel](#b-the-scope-panel)
* (C):  [the Results Panel](#c-the-results-panel)
* (D):  [the Deck Panel](#d-the-deck-panel)
* Bottom: [the Run Bar](#the-run-bar)

**NOTE**:  The author uses `grimoire` in a terminal window of height ~60 lines
and width ~200 columns.  If your terminal is significantly smaller than this,
you may run into difficulties rendering parts of the display.  (For example,
the card panel is hard-coded to be 30x52.)

Please see the [Grimoire User Input document](../controller/README.md) for the
keyboard commands that will allow you to navigate this interface.  NOTE:  This
document only provides a brief description of the purpose of each section of
the interface.

## The Search Bar

The user's cursor will initially be placed in the search bar at the top of the
window.  The user may search for cards using the [Grimoire Search
Language](../search/README.md).  For example, typing `t:dragon c<=ur` will
find all cards that are dragons in Izzet/Prismari colors.  The results of the
search will show up in the middle (C) panel, [the "results
panel"](#c-the-results-panel), and your focus will automatically be moved
there.

If there are any problems parsing the user's query, the error message will be
printed to the bottom right of the screen.

## (C) The Results Panel

Typing `/` from the results panel will return you to [the search
bar](#the-search-bar).

The results panel will display a list of cards that matched the user's search.
The results can be sorted by Name, Color, or Mana Value (CMC).  You can see
these options in the bottom left panel (B), [the "scope
panel"](#b-the-scope-panel).

When the user highlights a card in the results list, the card information --
such as name, mana cost, type, etc. -- will show up in the top left panel (A),
[the "card panel"](#a-the-card-panel).  The user may add or remove cards
to/from the deck from this panel by hitting `+` or `-`.

Typing the `TAB` key from this panel will change the focus to [the deck
panel](#d-the-deck-panel).  This will swap the results and deck panels so that
the deck panel will be in the middle and the results will be on the right.
When not in the search or run bars, the user focus will always rest in the
middle panel.

Finally, you may hit the `:` key to drop to [the run bar](#the-run-bar).  From
there you can quit the program and return to the terminal.

## (B) The Scope Panel

The scope panel shows options for the primary (middle) panel.  When the
results panel is in focus, the scope panel will provide options for sorting
results by Name, Color, or Mana Value (CMC).  When the deck panel is in focus,
the scope panel will provide options for grouping cards by Mana Value (CMC) or
Type.

## (A) The Card Panel

The card panel shows the following information for a highlighted card:
* Name
* Mana Cost
* Type(s)
* Rarity
* Oracle Text
* Name and Mana Cost of the other side of the card (if a Modal DFC)
* Set
* Power/Toughness (if a Creature or Vehicle)

## (D) The Deck Panel

The deck panel will list the contents of the user's deck.  The user may add or
remove cards to/from the deck by hitting `+` or `-`.  The user may also add
cards to the deck from [the results panel](#c-the-results-panel).  Typing the
`TAB` key will return focus to the results panel.

If the deck has been modified, the filename of the deck at the top of the
panel will be preceded by a `*`.

[The scope panel](#b-the-scope-panel) will give the user the option of
grouping the cards in their deck by Mana Value (CMC) or Type.

The bottom of the deck panel will show the deck's sideboard, which is empty by
default.  The sideboard is always sorted by mana value.

Like the results panel, [the card panel](#a-the-card-panel) will show
information for the highlighted card in the user's deck, and typing `/` or `:`
will take the user to the search or run bar, respectively.

## The Run Bar

The run bar executes several commands, like saving or importing decks.  See
the [Grimoire User Input guide](../controller/README.md#the-run-bar) for the
complete list of commands.

Notifications of successful commands or errors will be printed in the bottom
right of the screen.
