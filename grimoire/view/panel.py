from curses.textpad import rectangle
from .type_stubs import _CursesWindow
import curses
import logging
logger = logging.getLogger(__name__)


class Panel(object):
    def __init__(self, screen: _CursesWindow, height: int, width: int, y: int, x: int) -> None:
        rectangle(screen, y, x, y + height - 1, x + width - 1)
        logger.info('  R: {}, {}, {}, {}'.format(y, x, y + height - 1, x + width - 1))
        self.height = height - 2
        self.width = width - 2
        self.win = curses.newwin(self.height, self.width, y + 1, x + 1)

    def refresh(self) -> None:
        self.win.refresh()
