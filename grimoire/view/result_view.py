from .base_view import BaseView
from .display_listing import DisplayListing
from .panel import Panel
from typing import Optional, Sequence
import logging
logger = logging.getLogger(__name__)


class ResultView(BaseView):
    DEFAULT_TITLE = 'NO Results'

    def __init__(self, panel: Panel) -> None:
        self._data: Sequence[DisplayListing] = []
        super().__init__(panel, title = self.DEFAULT_TITLE)

    def update_data(self, listings: Sequence[DisplayListing]) -> None:
        """display the new card listing"""
        self._data = listings
        self.start_idx = 0
        self.idx = 0
        self._draw_data(self._data)

    def move_up(self) -> None:
        """scroll up"""
        if self.idx <= 0:
            return
        if self.idx == self.start_idx:
            self.start_idx -= 1
            self.idx -= 1
            self._draw_data(self._data)
        else:
            line = self._line_number()
            self.idx -= 1
            self._draw_data(self._data, [line, line - 1])

    def move_down(self) -> None:
        """scroll down"""
        if self.idx >= len(self._data) - 1:
            return
        if self._on_last_line():
            self.start_idx += 1
            self.idx += 1
            self._draw_data(self._data)
        else:
            line = self._line_number()
            self.idx += 1
            self._draw_data(self._data, [line, line + 1])

    def goto_top(self) -> None:
        """jump to the first card"""
        self.idx = self.start_idx = 0
        self._draw_data(self._data)

    def goto_bottom(self) -> None:
        """jump to the last card"""
        self.idx = len(self._data) - 1
        if len(self._data) < self._height - 1:
            self.start_idx = 0
        else:
            self.start_idx = self.idx - self._height + 2
        self._draw_data(self._data)

    def replace(self, listing: DisplayListing, idx: Optional[int] = None) -> None:
        """replace the listing at idx"""
        if idx is None:
            self._data[self.idx].left = listing.left
            self._data[self.idx].right = listing.right
            self._draw_line(self._line_number(), listing, True)
        else:
            self._data[idx].left = listing.left
            self._data[idx].right = listing.right
            self._draw_data(self._data)

    def temp_replace(self, tmp: DisplayListing) -> None:
        """temporarily overwrite the highlighted line with a new listing"""
        self._draw_line(self._line_number(), tmp, highlight=True)

    # PRIVATE METHODS

    def _set_panel(self, new_panel: Panel) -> None:
        super()._set_panel(new_panel)
        if self._data:
            self._draw_data(self._data)
        else:
            self._window.erase()
            if self._title:
                self._draw_title(self._title)

    def _draw_data(self, listings: Sequence[DisplayListing], lines: Sequence[int] = []) -> None:
        start = self.start_idx
        end = start + min(len(listings[start:]), self._height - 1)
        line = 1
        hl = self.idx - start + 1
        for item in listings[start:end]:
            if not lines or line in lines:
                self._draw_line(line, item, hl == line)
            line += 1
        while line < self._height:
            if not lines or line in lines:
                self._insstr(line, 0, ' ' * self._width)
            line += 1

    def _on_last_line(self) -> bool:
        return self._line_number() == self._height - 1

    def _line_number(self) -> int:
        return self.idx - self.start_idx + 1
