from .display_listing import DisplayListing
from .mana_display import ManaDisplay
from .panel import Panel
from typing import Dict, Optional, Sequence, Tuple
import curses


class BaseView(object):
    def __init__(self, panel: Panel, title: Optional[str] = None) -> None:
        self.row = 0
        self.line = 0
        self._title = title
        self.panel = panel

    @property
    def panel(self) -> Panel:
        return self._panel

    @panel.setter
    def panel(self, new_panel: Panel) -> None:
        self._set_panel(new_panel)

    @property
    def title(self) -> Optional[str]:
        return self._title

    @title.setter
    def title(self, new_title: str) -> None:
        self._set_title(new_title)

    # PRIVATE METHODS

    def _set_panel(self, new_panel: Panel) -> None:
        self._panel = new_panel
        self._window = new_panel.win
        self._height = new_panel.height
        self._width = new_panel.width
        if self._title is not None:
            self._draw_title(self._title)

    def _set_title(self, new_title: str) -> None:
        self._title = new_title
        self._draw_title(self._title)

    def _draw_title(self, title: str, line: int = 0) -> None:
        '''add a title to the panel'''
        self._draw_divider(line, title, '=')

    def _draw_section(self, section: str, line: int) -> None:
        self._draw_divider(line, section, '-')

    def _draw_divider(self, line: int, name: str, marker: str) -> None:
        assert len(marker) == 1
        total = self._width - len(name) - 3
        bars = marker * (total // 2)
        if total % 2:
            string = '{}[ {} ]{}'.format(bars, name, bars)
        else:
            string = '{}[ {} ]{}'.format(bars[:-1], name, bars)
        self._window.insstr(line, 0, string)

    def _draw_line(self, line: int, dl: DisplayListing, highlight: bool = False) -> None:
        self._insstr(line, 0, ' ' * self._width, highlight)
        self._insert_items(line, 0, list(dl.left.items())[::-1], highlight)
        cursor = self._right_aligned_column(dl.right)
        self._insert_items(line, cursor, list(dl.right.items()), highlight)

    def _insert_items(self, line: int, col: int, items: Sequence[Tuple[str, str]],
                      highlight: bool = False) -> None:
        for key, string in items:
            self._insstr(line, col, ' ', highlight)
            if key == 'mana':
                ManaDisplay(string).insstr(self._window, line, col, highlight)
            else:
                self._insstr(line, col, string, highlight, 'bold' in key)

    def _insstr(self, line: int, col: int, string: str,
                highlight: bool = False, bold: bool = False) -> None:
        """insert string with correct formatting"""
        layout = curses.color_pair(0)
        if highlight:
            layout |= curses.A_STANDOUT
        if bold:
            layout |= curses.A_BOLD
        self._window.insstr(line, col, string, layout)

    def _right_aligned_column(self, right: Dict[str, str]) -> int:
        """return the column for where to insert right-aligned text"""
        col = self._width + 1
        for key, value in right.items():
            col -= 1
            if key == 'mana':
                col -= len(ManaDisplay(value))
            else:
                col -= len(value)
        return 0 if col < 0 else col
