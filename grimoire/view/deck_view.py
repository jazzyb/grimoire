from .base_view import BaseView
from .display_listing import DisplayListing
from ..model.focus import Focus
from .panel import Panel
from typing import Dict, Optional, Sequence
import logging
logger = logging.getLogger(__name__)


class DeckView(BaseView):
    DEFAULT_TITLE = 'untitled.dec'

    def __init__(self, panel: Panel) -> None:
        self.idx: Optional[int] = None
        self.deck = []
        self.sb: Sequence[DisplayListing] = []
        super().__init__(panel, title = self.DEFAULT_TITLE)
        # set the boundaries of our window frame:
        self.first = 0
        self.last = self.md_height
        self.focus = Focus.MAIN

    @property
    def deck(self) -> Sequence[DisplayListing]:
        return self._deck

    @deck.setter
    def deck(self, deck: Sequence[DisplayListing]) -> None:
        self.i2l_map: Dict[int, int] = {}
        self._deck = deck

        index = 0
        line = 0
        for listing in self._deck:
            if 'blank' not in listing.left and 'section' not in listing.left:
                self.i2l_map[index] = line
                index += 1
            line += 1

    @property
    def md_height(self) -> int:
        """number of rows available to the maindeck"""
        h = self._height - 2
        return h - len(self.sb)

    def set_focus(self, new_focus: Focus, idx: Optional[int] = None) -> None:
        if self.focus == new_focus:
            return
        self.focus = new_focus
        self.idx = idx
        self._set_window_frame()
        self._draw_deck()

    def update(self, deck: Sequence[DisplayListing], idx: Optional[int],
               sideboard: Sequence[DisplayListing] = []) -> None:
        """update the deck view"""
        self.deck = deck
        self.last -= len(sideboard) - len(self.sb)
        self.sb = sideboard
        self.idx = idx
        self._set_window_frame()
        self._draw_deck()

    def update_indices(self, new_idx: int, indices: Sequence[int]) -> None:
        """only update the display for the cards at the given indices

        Used when we don't need to redraw the entire screen
        """
        self.idx = new_idx
        self._set_window_frame()
        self._draw_deck(indices)

    def temp_replace(self, tmp: DisplayListing) -> None:
        """temporarily overwrite the highlighted line with a new listing"""
        if self.idx is None:
            return
        row = self._row(self.idx)
        self._draw_line(row, tmp, highlight=True)

    # PRIVATE METHODS

    def _set_panel(self, panel: Panel) -> None:
        super()._set_panel(panel)
        if self.deck and self.idx is None:
            # we must have just swapped panels -- start at the top
            self.idx = 0
        self._set_window_frame()
        self._draw_deck()

    def _set_window_frame(self) -> None:
        if self.idx is None or self.focus == Focus.SIDE:
            return

        line = self._find_line_by_index(self.idx) if self.idx else 0
        if line < self.first:
            self.first = line
            self.last = self.first + self.md_height
        elif line > self.last:
            self.last = line
            self.first = self.last - self.md_height

    def _find_line_by_index(self, idx: int) -> int:
        if self.focus == Focus.MAIN:
            return self.i2l_map[idx]
        else:
            return self.md_height + idx + 4

    def _row(self, idx: int) -> int:
        """return the window row for the given card index"""
        row = self._find_line_by_index(idx)
        return row - self.first + 1 if self.focus == Focus.MAIN else row

    def _draw_deck(self, indices: Sequence[int] = []) -> None:
        if not self.deck:
            self._erase()
            return

        if self.idx is None:
            # a card has been removed from the result list
            self._erase()

        current_row = self._row(self.idx) if self.idx is not None else 0
        logger.debug('current: {}'.format(current_row))
        if current_row in (0, 1, 2, self.md_height + 1):
            # if there is any chance we have changed the boundaries of the
            # frame (i.e. first or last), then redraw the whole window
            self._erase()
            rows = []
        else:
            rows = [self._row(i) for i in indices]

        row = 1
        for listing in self.deck[self.first:self.last + 1]:
            logger.debug('row: {} -> {}'.format(row, repr(listing)))
            if 'blank' in listing.left:
                self._draw_blank_line(row)
            elif 'section' in listing.left:
                self._label_section(row, listing.left['section'])
            elif not rows or row in rows:
                self._draw_line(row, listing, highlight = (row == current_row))
            row += 1

        if not rows:
            while row < self._height:
                self._draw_blank_line(row)
                row += 1

        # draw sideboard
        row = self._height - len(self.sb)
        for listing in self.sb:
            logger.debug('row (sb): {} -> {}'.format(row, repr(listing)))
            if 'blank' in listing.left:
                self._draw_blank_line(row)
            elif 'title' in listing.left:
                self._draw_title(listing.left['title'], row)
            elif not rows or row in rows:
                self._draw_line(row, listing, highlight = (row == current_row))
            row += 1

    def _erase(self) -> None:
        self._window.erase()
        if self.title is not None:
            self._draw_title(self.title)

    def _draw_blank_line(self, row: int) -> None:
        self._insstr(row, 0, ' ' * self._width)

    def _label_section(self, row: int, section: str) -> None:
        self._draw_section(section, row)
