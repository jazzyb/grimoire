from .type_stubs import _CursesWindow
from typing import Dict
import curses
import re


class ManaDisplay(object):
    COLOR_PAIRS: Dict[str, int] = {}

    def __init__(self, mana_cost: str) -> None:
        self.__class__._init_color_pairs()
        self.pips = re.findall(r'{(.+?)}', mana_cost)

    def __len__(self) -> int:
        return len(' '.join(x for x in self.pips)) + 1

    def insstr(self, window: _CursesWindow, line: int, col: int, highlight: bool = False) -> int:
        for pip in self.pips:
            for c in pip:
                layout = self._print_char(window, line, col, c, highlight)
                col += 1
            window.insstr(line, col, ' ', layout)
            col += 1
        return len(self)

    # PRIVATE METHODS

    @classmethod
    def _init_color_pairs(cls) -> None:
        if cls.COLOR_PAIRS:
            return
        cls.COLOR_PAIRS['DEFAULT'] = curses.color_pair(0)
        curses.init_pair(1, curses.COLOR_WHITE, curses.COLOR_BLACK)
        cls.COLOR_PAIRS['WHITE'] = curses.color_pair(1) | curses.A_BOLD
        curses.init_pair(2, curses.COLOR_BLUE, curses.COLOR_BLACK)
        cls.COLOR_PAIRS['BLUE'] = curses.color_pair(2) | curses.A_BOLD
        curses.init_pair(3, curses.COLOR_MAGENTA, curses.COLOR_BLACK)
        cls.COLOR_PAIRS['BLACK'] = curses.color_pair(3) | curses.A_BOLD
        curses.init_pair(4, curses.COLOR_RED, curses.COLOR_BLACK)
        cls.COLOR_PAIRS['RED'] = curses.color_pair(4) | curses.A_BOLD
        curses.init_pair(5, curses.COLOR_GREEN, curses.COLOR_BLACK)
        cls.COLOR_PAIRS['GREEN'] = curses.color_pair(5) | curses.A_BOLD
        curses.init_pair(6, curses.COLOR_YELLOW, curses.COLOR_BLACK)
        cls.COLOR_PAIRS['COLORLESS'] = curses.color_pair(6)

    def _print_char(self, window: _CursesWindow, line: int, col: int, c: str, hl: bool) -> int:
        color = {'W': 'WHITE',
                 'U': 'BLUE',
                 'B': 'BLACK',
                 'R': 'RED',
                 'G': 'GREEN',
                 '/': 'DEFAULT'}.get(c, 'COLORLESS')
        layout = self.COLOR_PAIRS[color]
        if hl:
            layout |= curses.A_STANDOUT
        window.insstr(line, col, c, layout)
        return layout
