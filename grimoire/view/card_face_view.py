from ..data.card import Card
from .mana_display import ManaDisplay
from .panel import Panel
from typing import List, Optional


class CardFaceView(object):
    def __init__(self, panel: Panel) -> None:
        self.panel = panel
        self.window = panel.win
        self.height = panel.height
        self.width = panel.width
        self._draw_default_back()

    def update(self, card: Optional[Card] = None, back: Optional[Card] = None) -> None:
        self.window.erase()
        if card is None:
            self._draw_default_back()
            return
        self.line = 0
        self._draw_name(card)
        self._draw_mana(card)
        self._draw_type(card)
        self._draw_rarity(card)
        self._draw_oracle(card)
        if back is not None:
            self._draw_back(back)
        self._draw_set(card)
        self._draw_power_toughness_loyalty(card)

    def _draw_name(self, card: Card) -> None:
        name = self._display_name(card)
        self.window.addstr(self.line, 0, name)
        self.line += len(name) // self.width + 1

    def _draw_mana(self, card: Card) -> None:
        mana = ManaDisplay(card.mana)
        mana.insstr(self.window, self.line, self.width - len(mana))
        self.line += 1

    def _draw_type(self, card: Card) -> None:
        self.window.insstr(self.line, 0, '-' * self.width)
        self.line += 1
        self.window.insstr(self.line, 0, card.types)
        self.line += len(card.types) // self.width + 1

    def _draw_rarity(self, card: Card) -> None:
        self.window.insstr(self.line, self.width - 3, '[' + card.rarity[0].upper() + ']')
        self.line += 1

    def _draw_oracle(self, card: Card) -> None:
        self.window.insstr(self.line, 0, '-' * self.width)
        self.line += 2
        self.window.addstr(self.line, 0, self._wrap_text(card.oracle))

    def _draw_back(self, back: Card) -> None:
        line = self.height - 3
        name = self._display_name(back) + '   '
        self.window.insstr(line, 0, name)
        mana = ManaDisplay(back.mana)
        mana.insstr(self.window, self.height - 3, len(name))

    def _display_name(self, card: Card) -> str:
        if card.layout == 'modal_dfc':
            return '[{}] {}'.format('<' if card.is_back else '>', card.name)
        elif card.layout == 'transform':
            return '[{}] {}'.format('☾' if card.is_back else '☉', card.name)
        return card.name

    def _draw_set(self, card: Card) -> None:
        self.window.insstr(self.height - 2, 0, '-' * self.width)
        self.window.insstr(self.height - 1, 0, card.set_id.upper())

    def _draw_power_toughness_loyalty(self, card: Card) -> None:
        if card.power and card.toughness:
            pt = '{} / {}'.format(card.power, card.toughness)
            self.window.insstr(self.height - 1, self.width - len(pt), pt)
        elif card.loyalty:
            loyalty = card.loyalty + ' '
            self.window.insstr(self.height - 1, self.width - len(loyalty), loyalty)

    def _wrap_text(self, text: str) -> str:
        lines: List[str] = []
        for line in text.split('\n'):
            words = ''
            for word in line.split():
                if len(words.split('\n')[-1] + ' ' + word) < self.width:
                    words += ' ' + word
                else:
                    words += '\n' + word
            lines.append(words)
        return '\n\n'.join(lines)

    # NOTE:  This ascii picture relies on the default HxW of the card
    # panel to be 30x52.
    BACK = (" +----------------------------------------------+ !"
            " | o                                          o | !"
            " |                                              | !"
            " |                                              | !"
            " |       M   M       A      GGG    II    CCC    | !"
            " |      M M M M     A A    G       II   C       | !"
            " |     M   M   M   AAAAA   G   G   II   C       | !"
            " |    M     M   M A     A   GGG    II    CCCCC  | !"
            " |   M           M                              | !"
            " |  M             M     The  Gathering (tm)     | !"
            " |                                              | !"
            " |                                              | !"
            " |                                              | !"
            " |                      W                       | !"
            " |                                              | !"
            " |                G           U                 | !"
            " |                                              | !"
            " |                                              | !"
            " |                  R       B                   | !"
            " |                                              | !"
            " |                                              | !"
            " |                                              | !"
            " |                                              | !"
            " |           +---------------------+            | !"
            " |           | D E C K M A S T E R |            | !"
            " |           +---------------------+            | !"
            " | o                                          o | !"
            " +----------------------------------------------+ ")

    def _draw_default_back(self) -> None:
        for line, string in enumerate(self.BACK.split('!')):
            self.window.insstr(line, 0, string)
