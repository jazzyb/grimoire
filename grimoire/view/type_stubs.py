from typing import Any, TYPE_CHECKING
if TYPE_CHECKING:
    from curses import _CursesWindow as _CursesWindow
else:
    _CursesWindow = Any
