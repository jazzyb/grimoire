from argparse import Namespace
from .card_face_view import CardFaceView
from ..controller.search_bar import SearchBar
from ..controller.run_bar import RunBar
from .deck_view import DeckView
from .panel import Panel
from .result_view import ResultView
from .scope_view import ScopeView
from .type_stubs import _CursesWindow
from typing import Optional
import curses
import logging
logger = logging.getLogger(__name__)


class InterfaceWindow(object):
    def __init__(self, screen: _CursesWindow, lines: int = -1, cols: int = -1) -> None:
        self.win = screen
        self.X = cols if cols > 0 else curses.COLS
        self.Y = lines if lines > 0 else curses.LINES
        logger.info('Lines: {}; Cols: {}'.format(self.Y, self.X))
        self._make_bars()
        self._make_panels()
        self._card_face_view: Optional[CardFaceView] = None
        self._scope_view: Optional[ScopeView] = None
        self._result_view: Optional[ResultView] = None
        self._deck_view: Optional[DeckView] = None
        self.win.refresh()
        self.refresh()

    def refresh(self) -> None:
        self.card_panel.refresh()
        self.scope_panel.refresh()
        self.primary.refresh()
        self.secondary.refresh()

    def getkey(self) -> str:
        return self.win.getkey()

    def search(self) -> str:
        return self.searchbar.edit()

    def execute(self, is_deck_saved: bool = True) -> Namespace:
        return self.runbar.parse(is_deck_saved)

    def info(self, message: str) -> None:
        logger.info('msg: ' + message)
        self._notify(message)

    def error(self, message: str) -> None:
        logger.error(message)
        self._notify(message)

    @property
    def card_face_view(self) -> CardFaceView:
        if self._card_face_view is None:
            self._card_face_view = CardFaceView(self.card_panel)
        return self._card_face_view

    @property
    def scope_view(self) -> ScopeView:
        if self._scope_view is None:
            self._scope_view = ScopeView(self.scope_panel)
        return self._scope_view

    @property
    def result_view(self) -> ResultView:
        if self._result_view is None:
            self._result_view = ResultView(self.primary)
        return self._result_view

    @property
    def deck_view(self) -> DeckView:
        if self._deck_view is None:
            self._deck_view = DeckView(self.secondary)
        return self._deck_view

    def swap_panels(self) -> None:
        self.result_view.panel, self.deck_view.panel = self.deck_view.panel, self.result_view.panel

    # PRIVATE METHODS

    CARD_WIDTH = 52
    CARD_HEIGHT = 30
    SRCH_PROMPT = 'Search: '
    EXEC_PROMPT = 'run: '

    def _make_bars(self) -> None:
        self.searchbar = SearchBar(self.win, self.SRCH_PROMPT, 0, 0, self.X)
        self.runbar = RunBar(self.win, self.EXEC_PROMPT, self.Y - 1, 0, self.X // 2)
        self.win.addstr(self.Y - 1, self.X // 2, '|')

    def _make_panels(self) -> None:
        logger.info('Card View:')
        self.card_panel = Panel(self.win, self.CARD_HEIGHT, self.CARD_WIDTH, 1, 0)
        logger.info('Scope View:')
        self.scope_panel = Panel(self.win, self.Y - self.CARD_HEIGHT - 2, self.CARD_WIDTH,
                                 self.CARD_HEIGHT + 1, 0)
        logger.info('Primary Panel:')
        self.primary = Panel(self.win, self.Y - 2, self._primary_width(), 1, self.CARD_WIDTH)
        logger.info('Secondary Panel:')
        self.secondary = Panel(self.win, self.Y - 2, self._secondary_width(),
                               1, self._secondary_start())

    def _primary_width(self) -> int:
        '''calculate the width of the secondary panel'''
        return self.X - self.CARD_WIDTH - self._secondary_width()

    def _secondary_width(self) -> int:
        '''calculate the width of the primary panel'''
        return (self.X - self.CARD_WIDTH) // 2

    def _secondary_start(self) -> int:
        '''calculate the x-start of the secondary panel'''
        return self._primary_width() + self.CARD_WIDTH

    def _notify(self, message: str) -> None:
        y = self.Y - 1
        x = (self.X // 2) + 1
        self.win.insstr(y, x, ' ' * (self.X - x))
        self.win.insstr(y, self.X - len(message) - 1, message)
