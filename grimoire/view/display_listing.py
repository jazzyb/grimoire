from collections import OrderedDict
from typing import Any


class DisplayListing(object):
    def __init__(self) -> None:
        self.left: OrderedDict[str, str] = OrderedDict()
        self.right: OrderedDict[str, str] = OrderedDict()

    def __eq__(self, other: Any) -> bool:
        try:
            left = list(self.left.items()) == list(other.left.items())
            right = list(self.right.items()) == list(other.right.items())
        except AttributeError:
            raise NotImplementedError
        return left and right

    def __repr__(self) -> str:
        left = self.left.items()
        right = self.right.items()
        return 'DisplayListing(left: {}; right: {})'.format(left, right)
