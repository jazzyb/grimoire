from .base_view import BaseView
from .display_listing import DisplayListing
from typing import List, Sequence
import curses


class ScopeView(BaseView):
    def fill(self, listings: Sequence[DisplayListing], line: int) -> None:
        self.line = line
        self._display_listings(listings)

    def select(self, line: int) -> None:
        self._draw_option(self.line, self.options[self.line - 1])
        self.line = line
        self._draw_option(self.line, self.options[self.line - 1], highlight = True)

    def _display_listings(self, listings: Sequence[DisplayListing]) -> None:
        self._window.erase()
        if self._title is not None:
            self._draw_title(self._title)
        line = 1
        self.options: List[Sequence[str]] = []
        for left in [x.left for x in listings]:
            self.options.append(list(left.values()))
            self._draw_option(line, self.options[-1], line == self.line)
            line += 1

    def _draw_option(self, line: int, words: Sequence[str], highlight: bool = False) -> None:
        layout = curses.color_pair(0)
        if highlight:
            layout |= curses.A_STANDOUT
        string = ' '.join(words)
        string += ' ' * (self._width - len(string))
        self._window.insstr(line, 0, string, layout)
