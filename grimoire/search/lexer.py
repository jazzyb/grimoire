"""Handle lexing of Grimoire search language"""

from typing import List, Sequence


SEPARATOR = (' ',)
OPERATOR = ('<', '>', '=', ':')


class Token(object):
    """Tokens are prodcued by Lexer objects from query sentences.

    They have four parts:
    - negate: whether or not to negate the command
    - name: the name of the query command
    - value: the value(s) of the command
    - operator: defines how the command relates to its value

    The intended usage is to initialize a token with its first character, and
    then add() each new character to it.  This insures its parts are correctly
    loaded.
    """

    def __init__(self, name: str) -> None:
        """Initialize token with its first character(s)"""
        if name.startswith('-'):
            self.negate = True
            self.name = name[1:]
        else:
            self.negate = False
            self.name = name
        self.operator = ''
        self.value = ''

    def add(self, ch: str) -> None:
        """Add a single character to the token"""
        if ch in OPERATOR:
            if self.value:
                raise SyntaxError('Too many operators for {}'.format(self.name))
            self.operator += ch
        elif self.operator:
            self.value += ch
        else:
            self.name += ch


def tokenize(sentence: str) -> Sequence[Token]:
    """Lexer for the Grimoire search language

    Turns a query string of characters into a sequence of tokens that can
    be parsed.  Splits commands by spaces and handles quoted strings.
    Handles ( and ) as special cases.
    """
    tokens: List[Token] = []
    new_token = True
    quotation = False
    for ch in sentence:
        if quotation:
            if ch == '"':
                new_token = True
                quotation = False
            else:
                tokens[-1].add(ch)
        elif ch == '"':
            quotation = True
        elif ch in SEPARATOR:
            new_token = True
        elif ch in ('(', ')'):
            tokens.append(Token(ch))
            new_token = True
        elif new_token:
            tokens.append(Token(ch))
            new_token = False
        else:
            tokens[-1].add(ch)
    return tokens
