"""Handle parsing of Grimoire search language"""

from .lexer import Token
from enum import IntEnum
from ..error import GrimoireError
from typing import List, Sequence, Tuple, Union
import re

# Search language commands implemented so far (TODO):
# [x] or
# [x] t(ype)
# [x] r(arity)
# [x] o(racle)
# [x] s(et)
# [x] c(olor)
# [x] cmc
# [x] pow(er)
# [x] tou(ghness) -- pow>tou
# [x] kw (keyword)
# [\] m(ana)
# [ ] is:[permanent, party, vanilla, etc.]
# [ ] tag


class Command(object):
    """Base class for all objects that the parser generates"""

    def __init__(self, token: Token) -> None:
        self.token = token
        self.op = token.operator
        # escape `'`s
        self.name = token.name.replace("'", "''")
        self.value = token.value.replace("'", "''")

    def __str__(self) -> str:
        """Return a condition for a SQL where-clause"""
        raise NotImplementedError()

    def negate(self) -> str:
        return 'NOT ' if self.token.negate else ''


def parse(tokens: Sequence[Token]) -> Sequence[Union[Command, str]]:
    """Generates a sequence of "commands" from the lexer's tokens.

    Each command in the sequence can produce a SQL condition, which can be
    ANDed together in a where-clause.  For example, the query sentence `c:ru
    t:instant` will be parsed into a sequence of commands that will produce
    the condition:  "(color_r AND color_u) AND (types LIKE '%instant%')".
    """
    return [parse1(x) for x in tokens]


def parse1(token: Token) -> Union[Command, str]:
    """Parse a single token and generate a corresponding node"""
    _check_unknown_operator(token)
    _check_right_side(token)

    if token.name in ('(', ')', 'or'):
        return token.name.upper()
    if not token.operator:
        return Name(token)
    if token.name in ('s', 'set'):
        return SetId(token)
    if token.name in ('t', 'type'):
        return LikePattern(token, 'types')
    if token.name in ('o', 'oracle'):
        return LikePattern(token, 'oracle')
    if token.name in ('kw', 'keyword'):
        return LikePattern(token, 'keywords')
    if token.name in ('r', 'rarity'):
        return Rarity(token)
    if token.name in ('c', 'color'):
        return Color(token)
    if token.name == 'cmc':
        return ManaValue(token)
    if token.name in ('m', 'mana'):
        return ManaCost(token)
    if token.name in ('p', 'produces'):
        return ManaProduction(token)
    if token.name in ('pow', 'power', 'tou', 'toughness'):
        return PowerToughness(token)
    if token.name in ('loy', 'loyalty'):
        return Loyalty(token)
    raise UnknownSearchCommand(token.name)


def _check_unknown_operator(token: Token) -> None:
    if token.operator not in ('', ':', '=', '<', '>', '<=', '>='):
        raise UnknownOperatorError(token.operator, token.name)


def _check_right_side(token: Token) -> None:
    if token.operator and not token.value:
        msg = 'No right side to operate on: "{}{}"'.format(token.name, token.operator)
        raise GrimoireError(msg)

#
# Errors:
#


class UnknownOperatorError(GrimoireError):
    """This is not one of the 6 valid command operators"""

    def __init__(self, op: str, name: str) -> None:
        msg = 'Unknown operator "{}" for "{}"'.format(op, name)
        super().__init__(msg)


class UnknownSearchCommand(GrimoireError):
    """This is an unknown search command"""

    def __init__(self, cmd: str) -> None:
        self.cmd = cmd
        msg = 'Unknown search command "{}"'.format(cmd)
        super().__init__(msg)

#
# Command Subclasses:
#


class Name(Command):
    """Find cards that match name"""

    def __str__(self) -> str:
        if self.name[0] == '!':
            return "(name IS {}'{}')".format(self.negate(), self.name[1:])
        else:
            return "(name {}LIKE '%{}%')".format(self.negate(), self.name)


class SetId(Command):
    """Find cards from set"""

    def __init__(self, token: Token) -> None:
        if token.operator not in ('=', ':'):
            raise UnknownOperatorError(token.operator, 'set')
        super().__init__(token)

    def __str__(self) -> str:
        return "(set_id IS {}'{}')".format(self.negate(), self.value)


class LikePattern(Command):
    """Command for type, oracle, and keyword"""

    def __init__(self, token: Token, column: str) -> None:
        if token.operator not in ('=', ':'):
            raise UnknownOperatorError(token.operator, column)
        super().__init__(token)
        self.column = column

    def __str__(self) -> str:
        return "({} {}LIKE '%{}%')".format(self.column, self.negate(), self.value)


R = IntEnum('R', 'common uncommon rare mythic')


class Rarity(Command):
    """Find cards of certain rarities"""

    def __str__(self) -> str:
        return "(rarity {}IN ({}))".format(self.negate(), self._get_rarities())

    def _get_rarities(self) -> str:
        val = self._get_rarity_value()
        op = self._get_operator()
        return ', '.join("'" + r.name + "'" for r in R if getattr(r, op)(val))

    def _get_rarity_value(self) -> R:
        return {'c':        R.common,              # noqa: E241
                'u':        R.uncommon,            # noqa: E241
                'r':        R.rare,                # noqa: E241
                'm':        R.mythic,              # noqa: E241
                'common':   R.common,              # noqa: E241
                'uncommon': R.uncommon,            # noqa: E241
                'rare':     R.rare,                # noqa: E241
                'mythic':   R.mythic}[self.value]  # noqa: E241

    def _get_operator(self) -> str:
        return {':':  '__eq__',           # noqa: E241
                '=':  '__eq__',           # noqa: E241
                '<':  '__lt__',           # noqa: E241
                '>':  '__gt__',           # noqa: E241
                '<=': '__le__',           # noqa: E241
                '>=': '__ge__'}[self.op]  # noqa: E241


def _operator_to_integer_comparison(op: str, negate: bool = False) -> str:
    """Converts (and negates) command operators into integer comparisons"""
    if negate:
        return {':':  '<>',     # noqa: E241
                '=':  '<>',     # noqa: E241
                '<':  '>=',     # noqa: E241
                '>':  '<=',     # noqa: E241
                '<=': '>',      # noqa: E241
                '>=': '<'}[op]  # noqa: E241
    if op == ':':
        return '='
    return op


class Color(Command):
    """Find cards of a certain color combination"""

    def __init__(self, token: Token) -> None:
        if 'c' in token.value and token.value != 'c':
            raise GrimoireError('A card cannot be both colored and colorless')
        if 'm' in token.value and token.value != 'm':
            raise GrimoireError('Using "m" with other colors is not supported')
        try:
            int(token.value)
        except ValueError:
            for ch in token.value:
                if ch not in 'cwubrgm':
                    raise GrimoireError('Unrecognized color "{}"'.format(ch))
        super().__init__(token)

    def __str__(self) -> str:
        # multicolor and colorless are special cases
        if self.value == 'm':
            return self._handle_multicolor()
        if self.value == 'c':
            return self._handle_colorless()
        try:
            value = int(self.value)
        except ValueError:
            pass
        else:
            return self._handle_integer(value)

        include_colors = []
        exclude_colors = []
        for color in 'wubrg':
            if color in self.value:
                include_colors.append('color_' + color)
            else:
                exclude_colors.append('color_' + color)
        inc_clause = ' AND '.join(include_colors)
        exc_clause = ' OR '.join(exclude_colors) if exclude_colors else '0'

        # see test_colors() in test/test_parser.py for examples of how these work
        stmt = {'>':    '(({}) AND ({}))'.format(inc_clause, exc_clause),          # noqa: E241
                '=':    '(({}) AND NOT ({}))'.format(inc_clause, exc_clause),      # noqa: E241
                '<=':   '(NOT ({}))'.format(exc_clause),                           # noqa: E241
                '<':    '(NOT ({}) AND NOT ({}))'.format(inc_clause, exc_clause),  # noqa: E241
                '>=':   '({})'.format(inc_clause),                                 # noqa: E241
                ':':    '({})'.format(inc_clause)                                  # noqa: E241
                }[self.op]
        return self.negate() + stmt

    def _handle_multicolor(self) -> str:
        if self.op in (':', '=', '>', '>='):
            return '(num_colors {} 1)'.format('<=' if self.token.negate else '>')
        if self.op == '<':
            return '(num_colors {} 1)'.format('>' if self.token.negate else '<=')
        # token.operator == '<='
        return '(0)' if self.token.negate else '(1)'

    def _handle_colorless(self) -> str:
        return self._handle_integer(0)

    def _handle_integer(self, value: int) -> str:
        op = _operator_to_integer_comparison(self.op, self.token.negate)
        return '(num_colors {} {})'.format(op, value)


class ManaValue(Command):
    """Find cards of a particular converted mana cost"""

    def __init__(self, token: Token) -> None:
        try:
            int(token.value)
        except ValueError:
            raise GrimoireError('cmc must be a number')
        super().__init__(token)

    def __str__(self) -> str:
        op = _operator_to_integer_comparison(self.op, self.token.negate)
        return '(cmc {} {})'.format(op, self.value)


class ManaCost(Command):
    """Find cards with particular pips in their casting cost"""

    def __init__(self, token: Token) -> None:
        if '<' in token.operator:  # TODO < and <=
            msg = "'mana' doesn't currently support the {} operator".format(token.operator)
            raise GrimoireError(msg)
        super().__init__(token)

    def __str__(self) -> str:
        specific_costs, gmc = self._separate_costs(self.value)
        # see test_mana_cost() in test/test_parser.py for examples of how these work
        if self.op == '=':
            return self._eq(specific_costs, gmc)
        if self.op == '>':
            return self._gt(specific_costs, gmc)
        # self.op in (':', '>=')
        return self._ge(specific_costs, gmc)

    def _separate_costs(self, cost: str) -> Tuple[Sequence[str], int]:
        gmc = 0
        ret_list: List[str] = []
        costs = re.findall(r'{.+?}|\d+|\w', cost)
        for x in sorted(_.replace('{', '').replace('}', '') for _ in costs):
            try:
                gmc += int(x)
            except ValueError:
                item = '{' + self._order_hybrid_colors(x) + '}'
                if ret_list and ret_list[-1].endswith(item):
                    ret_list[-1] += item
                else:
                    ret_list.append(item)
        return ret_list, gmc

    def _order_hybrid_colors(self, value: str) -> str:
        return {'u/w': 'w/u',
                'b/u': 'u/b',
                'r/b': 'b/r',
                'g/r': 'r/g',
                'w/g': 'g/w',
                'b/w': 'w/b',
                'r/u': 'u/r',
                'g/b': 'b/g',
                'w/r': 'r/w',
                'u/g': 'g/u'}.get(value, value)

    def _eq(self, costs: Sequence[str], gmc: int) -> str:
        like_stmt = self._mana_like_stmt(costs, gmc, '=')
        length_stmt = "length(mana) = {}".format(self._cost_len(costs, gmc))
        return "{}(({}) AND ({}))".format(self.negate(), like_stmt, length_stmt)

    def _ge(self, costs: Sequence[str], gmc: int) -> str:
        return "{}({})".format(self.negate(), self._mana_like_stmt(costs, gmc, '>='))

    def _gt(self, costs: Sequence[str], gmc: int) -> str:
        like_stmt = self._mana_like_stmt(costs)
        cost_len = self._cost_len(costs, gmc)
        if gmc:
            first = "gmc > {} AND length(mana) >= {}".format(gmc, cost_len)
            second = "gmc = {} AND length(mana) > {}".format(gmc, cost_len)
            length_stmt = "({}) OR ({})".format(first, second)
        else:
            length_stmt = "length(mana) > {}".format(cost_len)
        return "{}(({}) AND ({}))".format(self.negate(), like_stmt, length_stmt)

    def _mana_like_stmt(self, costs: Sequence[str], gmc: int = 0, op: str = '') -> str:
        mana_like = ["mana LIKE '%{}%'".format(x) for x in costs]
        if gmc:
            mana_like.append("gmc {} {}".format(op, gmc))
        return ' AND '.join(mana_like)

    def _cost_len(self, costs: Sequence[str], gmc: int) -> int:
        cost_len = len(''.join(costs))
        if gmc:
            cost_len += len(str(gmc)) + 2
        return cost_len


class ManaProduction(Command):
    """Find cards producing the given mana"""

    def __init__(self, token: Token) -> None:
        if token.operator != ':':
            raise GrimoireError("'produces' currently only supports the : operator")
        if 'm' in token.value and token.value != 'm':
            raise GrimoireError("Using 'm' with other colors is not supported")
        for ch in token.value:
            if ch not in 'cwubrgm':
                raise GrimoireError('Unrecognized color "{}"'.format(ch))
        super().__init__(token)

    def __str__(self) -> str:
        if self.value == 'm':
            op = '<=' if self.token.negate else '>'
            return "(LENGTH(REPLACE(produces, 'c', '')) {} 1)".format(op)
        colors = ' AND '.join("produces LIKE '%{}%'".format(x) for x in self.value)
        return '{}({})'.format(self.negate(), colors)


class PowerToughness(Command):
    """Find creatures and vehicles by power or toughness"""

    def __str__(self) -> str:
        op = _operator_to_integer_comparison(self.op, self.token.negate)
        left = self._expand_value(self.name)
        right = self._expand_value(self.value)
        return "(power <> '' AND toughness <> '' AND {} {} {})".format(left, op, right)

    def _expand_value(self, val: str) -> str:
        if val in ('pow', 'power'):
            return 'power'
        if val in ('tou', 'toughness'):
            return 'toughness'
        try:
            int(val)
        except ValueError:
            return "'" + val + "'"
        else:
            return val


class Loyalty(Command):
    """Find planeswalkers by initial loyalty"""

    def __str__(self) -> str:
        op = _operator_to_integer_comparison(self.op, self.token.negate)
        return "(loyalty <> '' AND loyalty {} {})".format(op, self.value)
