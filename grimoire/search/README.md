# Grimoire Search Language

The Grimoire search language is a query language for the Grimoire card
database.  The syntax is derived from a subset of
[Scryfall](https://scryfall.com/docs/syntax)'s.  If you are familiar with that
language, then you shouldn't have any issue with Grimoire.

However, you might notice some differences in the results.  For example,
Scryfall treats DFCs as a single card, whereas Grimoire counts them as two.
This means a query such as
[`set:stx color<=wb o:draw o:card`](https://scryfall.com/search?q=set%3Astx+color%3C%3Dwb+o%3Adraw+o%3Acard&unique=cards&as=grid&order=name)
on Scryfall will return
["Plargg, Dean of Chaos"](https://scryfall.com/card/stx/155/plargg-dean-of-chaos-augusta-dean-of-order),
a mono-red creature, because the dean on the back-face is white.  On the other
hand, the Grimoire result will include
[Eyetwitch](https://scryfall.com/card/stx/70/eyetwitch) and
[Guiding Voice](https://scryfall.com/card/stx/19/guiding-voice) because
Grimoire includes the reminder text (*Learn*: "...discard a card to draw a
card") in the oracle text.

# Syntax

Search commands take the form of `<COMMAND><OPERATOR><VALUE>`, where
`COMMAND` can be one of:

```
set / s
type / t
color / c
mana / m
cmc
rarity / r
oracle / o
power / pow
toughness / tou
loyalty / loy
keyword / kw
produces / p
```

`OPERATOR` may be one of `: = < <= > >=`, and `VALUE` depends entirely on the
command.

All terms are case-insensitive.  `t:sorcery c<=ur` is the same as `T:SORCERY C<=UR`.

All terms are implicit ANDs.  To OR use `( )` and `or`.  For example, `c<=ur
(t:sorcery or t:instant)`.

Spaces are not allowed between the command and its operator or between the
operator and value.  For example, to search for all cards that are white and
blue, you query `c=wu` NOT `c = wu`.

Spaces are allowed in the value with quotes.  For example, all creatures with
ETB effects can be searched with `t:creature o:"when ~ enters the battlefield"`.

## Negate

Any command may be negated by preceding it with `-`.  For example,
`-t:creature` will return all cards that are not creatures.

## Names

Grimoire treats any search term without an operator as a card name.  They will
match any part of the name.  For example, `alrund` will return "Alrund, God of
the Cosmos" and "Alrund's Epiphany".

Prepending `!` to the name will search for the card with *that* specific name.
The name is still case-insensitive.  For example, `!"didn't say please"`.

## Sets

Return cards from the given set.  Only accepts the `:` and `=` operators.
`set:stx` returns all Strixhaven cards.

## Types

Return cards of the given type.  Only accepts the `:` and `=` operators.
`t:creature` returns all creature spells.

## Colors

Return cards of the given colors:
* `c=wu` returns spells that are white and blue but no other colors
* `c>wu` returns spells that are white and blue and additionally something else
* `c<=br` returns cards that are black, red, or colorless
* `c<br` returns cards that are colorless, black, or red BUT NOT black and red
* `c>=gw` returns cards that are at least green and white
* `c:gw` is the same as `>=`

Return cards that are colorless: `color:c`

Return cards that are multicolor: `color:m`

Return cards that have a certain number of colors: ex.: `color>3`

## Mana Costs

Return cards with the given mana cost.  (These are the pips in the casting
cost of the card.)  Only accepts the following operators:
* `m=wr` returns cards with a casting cost exactly equal to `{R}{W}`
* `m>1wb` returns cards with either additional pips in the casting cost *or* a
  higher generic mana cost.  For example, `set:stx m>1wb` will return both
  [Extus, Oriq Overlord](https://scryfall.com/card/stx/149/extus-oriq-overlord-awaken-the-blood-avatar) and [Silverquill Command](https://scryfall.com/card/stx/232/silverquill-command).
* `m>=wb` returns cards containing both `{W}` and `{B}` in their casting cost
* `m:wb` is the same as `>=`

## Mana Values

Also known as "converted mana cost," hence the command `cmc`.  Returns cards
with the given converted mana cost.

## Rarities

Returns cards of the given rarity/rarities.  `r:common` returns only commons.
`r>uncommon` returns only rares and mythics.

## Oracle Text

Return cards matching the given card text.  `~` is a stand-in for a card's
name.  `o:"when ~ enters the battlefield"` returns all cards with ETB
triggers.  Only accepts the `:` and `=` operators.

## Power / Toughness

Return cards with the given power or toughness.
`t:creature tou>=4` returns all creatures with toughness 4 or greater.
`t:creature pow>tou` returns all creatures whose power is greater than their
toughness.

## Loyalty

Return cards with the given initial loyalty.
`loy>4` returns all planeswalkers with an initial loyalty of more than 4.

## Keywords

Return cards with the given keywords.  Only accepts the `:` and `=` operators.
`kw:haste` returns all spells with `haste`.

## Produces

Return cards that produce the given colors of mana.  Only accepts the ':'
operator.
`p:wu` returns all cards that produce both white *and* blue mana.
