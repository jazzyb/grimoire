from .parser import Command
from typing import List, Sequence, Union


def where_clause(commands: Sequence[Union[Command, str]]) -> str:
    """Convert the Grimoire commands to a SQL WHERE-clause"""
    if not commands:
        return '0'

    clause: List[str] = []
    for cmd in [str(x) for x in commands]:
        if cmd in (')', 'OR'):
            _pop_and(clause)
        clause.append(cmd)
        if cmd not in ('(', 'OR'):
            clause.append('AND')
    _pop_and(clause)
    return ' '.join(clause)


def _pop_and(clause: List[str]) -> None:
    if clause[-1] == 'AND':
        clause.pop()
