from argparse import ArgumentParser, Namespace
from datetime import datetime, timezone
from grimoire.app import GrimoireApp
from grimoire.data.bulk_data import BulkData
from grimoire.data.database import Database
from grimoire.data.downloader import ConnectionError, Downloader
from grimoire.view.type_stubs import _CursesWindow
from typing import Optional, Sequence
import os
import sys
import logging
logger = logging.getLogger(__name__)


LOG_FILE = 'grimoire.log'
LOG_MSG_FORMAT = '%(asctime)s: [%(levelname)s] %(name)s +%(lineno)s: %(message)s'
DEFAULT_BULK_DATA = 'scryfall-latest.json'
DEFAULT_CARD_DATA = 'scryfall-latest.sqlite'


def parse_args() -> Namespace:
    parser = ArgumentParser(description='Grimoire M:TG deck builder')
    parser.add_argument('--open', help='the deck file to open', dest='deck')
    parser.add_argument('--database', help='card database', default=DEFAULT_CARD_DATA)
    parser.add_argument('-v', '--verbosity', help='increase the logging output',
                        action='count', default=0)

    # download
    subparsers = parser.add_subparsers(dest='command')
    download = subparsers.add_parser('download')
    download.add_argument('--format', help='only cards legal in this format', default='future')
    download.add_argument('--sets', help='comma separated list of sets to include', default='')
    download.add_argument('--output', help='the file to write the database to',
                          default=DEFAULT_CARD_DATA)
    download.add_argument('--save-json', help='do not remove the JSON bulk data file',
                          action='store_true')

    return parser.parse_args()


def initialize_database(legal_fmt: str, sets: Sequence[str], filename: str, save: bool) -> None:
    """initialize the Grimoire card database"""
    try:
        outfile = download_bulk_data(utc_timestamp(filename))
    except ConnectionError:
        print('Error: Cannot establish a connection to Scryfall', file=sys.stderr)
        sys.exit(1)
    if outfile is None:
        print('Grimoire database is up to date with latest Scryfall data\nNothing to do.')
        return

    print('Adding bulk data to the Grimoire database (this may take a few seconds)...')
    cards = BulkData(outfile, legal_fmt, sets).to_cards()
    with Database() as db:
        db.insert(cards)
        db.dump(filename)
    if not save:
        os.remove(outfile)
    print('Done.')


def download_bulk_data(db_ts: Optional[datetime]) -> Optional[str]:
    """download the Scryfall bulk data and return the bulk data filename"""
    dl = Downloader()
    if db_ts and dl.latest_ts <= db_ts:
        return None
    print('Getting Scryfall bulk data...')
    return dl.download_bulk_data(DEFAULT_BULK_DATA)


def utc_timestamp(filename: str) -> Optional[datetime]:
    """return the file timestamp as a UTC datetime object"""
    if os.path.exists(filename):
        return datetime.fromtimestamp(os.path.getmtime(filename), tz=timezone.utc)
    return None


def main(screen: _CursesWindow, db_filename: str, deck: Optional[str] = None) -> None:
    logger.info('!!! STARTING GRIMOIRE TUI !!!')
    app = GrimoireApp(screen, db_filename, deck)
    app.run()


if __name__ == '__main__':
    args = parse_args()
    if args.command == 'download':
        sets = [x.lower() for x in args.sets.split(',')] if args.sets else []
        initialize_database(args.format, sets, args.output, args.save_json)
    else:
        if args.verbosity == 0:
            logging.basicConfig(level=logging.CRITICAL)
        else:
            file_handler = logging.FileHandler(filename = LOG_FILE, encoding = 'UTF-8', mode = 'a+')
            logging.basicConfig(handlers = [file_handler],
                                format = LOG_MSG_FORMAT,
                                level = logging.INFO if args.verbosity == 1 else logging.DEBUG)

        if not os.path.exists(args.database):
            msg = ('Error: database "{}" does not exist.\n'
                   '       Make sure you are in the correct directory or try '
                   'running `grimoire download` first.').format(args.database)
            print(msg, file=sys.stderr)
            sys.exit(1)

        import curses
        curses.wrapper(main, args.database, args.deck)
