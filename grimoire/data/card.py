"""Defines the card types for the Grimoire database"""

from typing import Any, Dict, NamedTuple
import re

JsonObject = Dict[str, Any]

CARD_TABLE = [
    ('scryfall_id', 'TEXT'),
    ('set_id', 'TEXT'),
    ('collector_number', 'TEXT'),
    ('name', 'TEXT UNIQUE COLLATE NOCASE'),
    ('rarity', 'TEXT'),
    ('mana', 'TEXT'),
    ('gmc', 'INTEGER'),
    ('cmc', 'INTEGER'),
    ('types', 'TEXT COLLATE NOCASE'),
    ('oracle', 'TEXT COLLATE NOCASE'),
    ('color_w', 'BOOLEAN'),
    ('color_u', 'BOOLEAN'),
    ('color_b', 'BOOLEAN'),
    ('color_r', 'BOOLEAN'),
    ('color_g', 'BOOLEAN'),
    ('num_colors', 'INTEGER'),
    ('keywords', 'TEXT'),
    ('produces', 'TEXT'),
    ('power', 'TEXT'),
    ('toughness', 'TEXT'),
    ('loyalty', 'TEXT'),
    ('layout', 'TEXT'),
    ('is_back', 'BOOLEAN'),
]


class Card(NamedTuple):
    scryfall_id: str
    set_id: str
    collector_number: str
    name: str
    rarity: str
    mana: str
    gmc: int
    cmc: int
    types: str
    oracle: str
    color_w: bool
    color_u: bool
    color_b: bool
    color_r: bool
    color_g: bool
    num_colors: int
    keywords: str
    produces: str
    power: str
    toughness: str
    loyalty: str
    layout: str
    is_back: bool


def _calc_generic_cost(cost: str) -> int:
    """Return the amount of generic mana in the casting cost"""
    m = re.search(r'{\d+}', cost)
    if m is None:
        return 0
    return int(m[0][1:-1])


def normal_card(obj: JsonObject) -> Card:
    """Process a normal card from JSON bulk data"""
    name = obj['name']
    return Card(
        scryfall_id = obj['id'],
        set_id = obj['set'],
        collector_number = obj['collector_number'],
        name = name,
        rarity = obj['rarity'],
        mana = obj['mana_cost'],
        gmc = _calc_generic_cost(obj['mana_cost']),
        cmc = obj['cmc'],
        types = obj['type_line'],
        oracle = obj['oracle_text'].replace(name, '~'),
        color_w = 'W' in obj['colors'],
        color_u = 'U' in obj['colors'],
        color_b = 'B' in obj['colors'],
        color_r = 'R' in obj['colors'],
        color_g = 'G' in obj['colors'],
        num_colors = len(obj['colors']),
        keywords = ' '.join(x.lower() for x in obj['keywords']),
        produces = ''.join(x.lower() for x in obj.get('produced_mana', [])),
        power = obj.get('power', ''),
        toughness = obj.get('toughness', ''),
        loyalty = obj.get('loyalty', ''),
        layout = obj['layout'],
        is_back = False,
    )


def _calc_cmc(cost: str) -> int:
    """Return the converted mana cost"""
    cmc = 0
    for match in re.findall(r'{\w+}', cost):
        if match[1:-1] == 'X':
            continue
        try:
            cmc += int(match[1:-1])
        except ValueError:
            cmc += 1
    return cmc


def modal_dual_face_card(obj: JsonObject, index: int) -> Card:
    """Process the front (0) or back (1) of an MDFC"""
    assert index in (0, 1)
    face = obj['card_faces'][index]
    name = face['name']
    return Card(
        scryfall_id = obj['id'],
        set_id = obj['set'],
        collector_number = obj['collector_number'],
        name = name,
        rarity = obj['rarity'],
        mana = face['mana_cost'],
        gmc = _calc_generic_cost(face['mana_cost']),
        cmc = _calc_cmc(face['mana_cost']),
        types = face['type_line'],
        oracle = face['oracle_text'].replace(name, '~'),
        color_w = 'W' in face['colors'],
        color_u = 'U' in face['colors'],
        color_b = 'B' in face['colors'],
        color_r = 'R' in face['colors'],
        color_g = 'G' in face['colors'],
        num_colors = len(face['colors']),
        keywords = ' '.join(x.lower() for x in obj['keywords']),
        produces = ''.join(x.lower() for x in obj.get('produced_mana', [])),
        power = face.get('power', ''),
        toughness = face.get('toughness', ''),
        loyalty = face.get('loyalty', ''),
        layout = obj['layout'],
        is_back = index == 1,
    )


def transform_card(obj: JsonObject, index: int) -> Card:
    """Process the front (0) or back (1) of a Transform card"""
    assert index in (0, 1)
    face = obj['card_faces'][index]
    name = face['name']
    return Card(
        scryfall_id = obj['id'],
        set_id = obj['set'],
        collector_number = obj['collector_number'],
        name = name,
        rarity = obj['rarity'],
        mana = face['mana_cost'],
        gmc = _calc_generic_cost(face['mana_cost']),
        cmc = obj['cmc'],
        types = face['type_line'],
        oracle = face['oracle_text'].replace(name, '~'),
        color_w = 'W' in face['colors'],
        color_u = 'U' in face['colors'],
        color_b = 'B' in face['colors'],
        color_r = 'R' in face['colors'],
        color_g = 'G' in face['colors'],
        num_colors = len(face['colors']),
        keywords = ' '.join(x.lower() for x in obj['keywords']),
        produces = ''.join(x.lower() for x in obj.get('produced_mana', [])),
        power = face.get('power', ''),
        toughness = face.get('toughness', ''),
        loyalty = face.get('loyalty', ''),
        layout = obj['layout'],
        is_back = index == 1,
    )
