# FIXME:  We construct the SELECT statement and WHERE clause with unsafe
# string formatting.  They should be changed to parameterized queries.
"""Grimoire card database"""

from .card import CARD_TABLE, Card
from ..error import GrimoireError
from typing import Optional, Sequence, Type
import sqlite3
import types
import logging
logger = logging.getLogger(__name__)
Connection = sqlite3.Connection


class Database(object):
    TABLE = 'cards'

    def __init__(self, db_filename: Optional[str] = None) -> None:
        """Initialize in-memory database"""
        self.mem_db = sqlite3.connect(':memory:')
        if db_filename:
            self.load(db_filename)
        else:
            self._create_table(self.mem_db)

    def __enter__(self) -> 'Database':
        return self

    def __exit__(self, exc_type: Optional[Type[BaseException]],
                 exc_val: Optional[BaseException],
                 exc_tb: Optional[types.TracebackType]) -> None:
        self.close()

    def close(self) -> None:
        self.mem_db.close()

    def load(self, db_filename: str) -> None:
        """Load databse on disk into memory"""
        src = sqlite3.connect(db_filename)
        self._backup(src, self.mem_db)
        src.close()

    def dump(self, db_filename: str) -> None:
        """Backup in-memory database to disk"""
        dst = sqlite3.connect(db_filename)
        self._backup(self.mem_db, dst)
        dst.close()

    def insert(self, cards: Sequence[Card]) -> None:
        """Insert cards into the database"""
        values = ','.join(['?'] * len(CARD_TABLE))
        stmt = 'INSERT OR IGNORE INTO {} VALUES ({})'.format(self.TABLE, values)
        with self.mem_db as db:
            db.executemany(stmt, cards)

    def fetch(self, where_clause: str, order_by: Optional[str] = None) -> Sequence[Card]:
        """Return cards matching where_clause"""
        cards = []
        query = 'SELECT * FROM {} WHERE {}'.format(self.TABLE, where_clause)
        if order_by is not None:
            query += ' ORDER BY {}'.format(order_by)
        logger.info(query)
        for row in self._execute(query):
            cards.append(Card(*row))
        return cards

    def fetchone(self, where_clause: str) -> Card:
        """Return a single card matching the where_clause"""
        query = 'SELECT * FROM {} WHERE {} LIMIT 1'.format(self.TABLE, where_clause)
        logger.debug(query)
        record = self._execute(query).fetchone()
        return Card(*record)

    def _backup(self, src: Connection, dst: Connection) -> None:
        self._drop_table(dst)
        with src:
            src.backup(dst)

    def _create_table(self, db: Connection) -> None:
        stmt = 'CREATE TABLE {} ('.format(self.TABLE)
        stmt += ','.join(col + ' ' + kind for col, kind in CARD_TABLE)
        stmt += ')'
        with db:
            db.execute(stmt)

    def _drop_table(self, db: Connection) -> None:
        with db:
            db.execute('DROP TABLE IF EXISTS {}'.format(self.TABLE))

    def _execute(self, query: str) -> sqlite3.Cursor:
        try:
            return self.mem_db.execute(query)
        except sqlite3.OperationalError as e:
            raise GrimoireError(str(e))
