"""Converts Scryfall bulk data into Grimoire cards"""

from .card import Card, JsonObject, normal_card, modal_dual_face_card, transform_card
from typing import List, Optional, Sequence
import json


JsonData = Sequence[JsonObject]


class BulkData(object):
    """Parse Scryfall bulk data file"""

    def __init__(self, bulk_data_file: str, legal: Optional[str] = 'future',
                 sets: Sequence[str] = []) -> None:
        """Read and filter the bulk data cards based on legal and sets"""
        with open(bulk_data_file, encoding='utf-8') as f:
            card_objs = json.load(f)
        self.raw_data = self._filter_data(card_objs, legal, sets)
        self.cards: List[Card] = []

    def to_cards(self) -> Sequence[Card]:
        """Return the cards from the bulk data"""
        if self.cards:
            return self.cards

        for o in self.raw_data:
            if o['layout'] == 'modal_dfc':
                self.cards.append(modal_dual_face_card(o, 0))
                self.cards.append(modal_dual_face_card(o, 1))
            elif o['layout'] == 'transform':
                self.cards.append(transform_card(o, 0))
                self.cards.append(transform_card(o, 1))
            else:
                self.cards.append(normal_card(o))
        return self.cards

    def _filter_data(self, card_objs: JsonData, fmt: Optional[str],
                     sets: Sequence[str]) -> JsonData:
        raw_data = []
        for o in card_objs:
            if fmt and o['legalities'][fmt] != 'legal':
                continue
            if sets and o['set'] not in sets:
                continue
            raw_data.append(o)
        return raw_data
