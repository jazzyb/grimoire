from datetime import datetime, timezone
from requests.exceptions import ConnectionError as ConnectionError  # noqa: F401
import json
import requests
import sys


class _ProgressBar(object):
    COMPRESSION_RATIO = 7.76  # the approximate size difference in gzipped JSON
    BAR_SIZE = 50

    def __init__(self, total: int) -> None:
        # magic number is approx. size difference in gzipped JSON
        self.total = total * self.COMPRESSION_RATIO
        self.so_far = 0
        self.progress = '|'

    def update(self, data_size: int) -> None:
        self.progress = {'|': '/', '/': '-', '-': '\\', '\\': '|'}[self.progress]
        self.so_far += data_size
        done = min(int(self.BAR_SIZE * (self.so_far / self.total)), self.BAR_SIZE)
        strings = ('=' * done, self.progress, ' ' * (self.BAR_SIZE - done - 1))
        self._write("\r Downloading... [%s%s%s]" % strings)

    def finish(self) -> None:
        self._write("\r    COMPLETE    [%s] \n" % ('=' * self.BAR_SIZE))

    def _write(self, data: str) -> None:
        sys.stdout.write(data)
        sys.stdout.flush()


class Downloader(object):
    SCRYFALL_DEFAULT_CARD_URI = 'https://api.scryfall.com/bulk-data/default-cards'

    def __init__(self) -> None:
        response = requests.get(self.SCRYFALL_DEFAULT_CARD_URI)
        data = json.loads(response.content)
        self.latest_uri = data['download_uri']
        self.latest_ts = self._get_datetime(data['updated_at'])

    def download_bulk_data(self, outfile: str) -> str:
        response = requests.get(self.latest_uri, stream = True)
        length = response.headers.get('content-length')
        assert type(length) is str
        with open(outfile, mode='wb') as f:
            bar = _ProgressBar(int(length))
            for data in response.iter_content(chunk_size = 4096):
                bar.update(len(data))
                f.write(data)
            bar.finish()
        return outfile

    def _get_datetime(self, ts: str) -> datetime:
        return datetime.strptime(ts[:-10], "%Y-%m-%dT%H:%M:%S").replace(tzinfo=timezone.utc)
