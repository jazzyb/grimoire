# Scryfall Bulk Data

Grimoire can download the Scryfall bulk card data.  The Downloader will pull
down the JSON data for all cards from the URI at
https://api.scryfall.com/bulk-data/default-cards and write them to
`sqlite-latest.json`.  Grimoire will then convert the JSON data for all
specified cards to SQLite (schema below) and write them to the database
`scryfall-latest.sqlite`.  By default the JSON data file is removed after
conversion.  See `python -m grimoire download --help` for more options.

# Schema

The schema for the Grimoire database:

```sql
CREATE TABLE cards (
	scryfall_id TEXT,
	set_id TEXT,
	collector_number TEXT,
	name TEXT UNIQUE COLLATE NOCASE,
	rarity TEXT,
	mana TEXT,
	gmc INTEGER,
	cmc INTEGER,
	types TEXT COLLATE NOCASE,
	oracle TEXT COLLATE NOCASE,
	color_w BOOLEAN,
	color_u BOOLEAN,
	color_b BOOLEAN,
	color_r BOOLEAN,
	color_g BOOLEAN,
	num_colors INTEGER,
	keywords TEXT,
	produces TEXT,
	power TEXT,
	toughness TEXT,
        loyalty TEXT,
	layout TEXT,
	is_back BOOLEAN
);
```

## Some Notes

* The columns `name`, `types`, and `oracle` are all defined as
  case-insensitive for searches.
* The `oracle` text replaces the name of the card with `~` to make, e.g.,
  searching for ETB effects across cards easier: `o:"when ~ enters the
  battlefield"`.
* `gmc` is the "generic mana cost" -- the amount of generic mana in the
  casting cost of the spell.  The field is not present in the Scryfall bulk
  data, but calculating it makes some of the `mana` commands easier to
  implement.
* Unlike Scryfall, the `cmc` for MDFCs is the mana value of the card *face*,
  not just the front of the card.  For example, the Grimoire query `set:khm o:scry
  cmc=2` will return among its results "Hakka, Whispering Raven".  The same
  search on Scryfall will not because Hakka's front face (Alrund) has a CMC of
  5.
* `is_back` is only true if this card is printed on the back of another card,
  e.g.  in the case of modal DFCs.
