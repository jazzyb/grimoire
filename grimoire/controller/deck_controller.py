from .base_controller import BaseController
from ..model.deck_model import DeckModel
from ..view.window import InterfaceWindow
import curses
import logging
logger = logging.getLogger(__name__)


def log_key_press(key: str) -> None:
    if len(key) == 1:
        logger.debug('key press: "{}" == {}'.format(key, ord(key)))
    else:
        logger.debug('key press: "{}"'.format(key))


class DeckController(BaseController):
    def __init__(self, window: InterfaceWindow, model: DeckModel) -> None:
        self.deck = model
        super().__init__(window)

    def control(self) -> str:
        self.deck.update_subordinate_panels()
        self.win.refresh()
        curses.curs_set(False)
        while True:
            c = self.win.getkey()
            log_key_press(c)
            if c in ('j', 'J', 'KEY_DOWN'):
                self.deck.move_down()
            elif c in ('k', 'K', 'KEY_UP'):
                self.deck.move_up()
            elif c in ('h', 'H', 'l', 'L', 'KEY_LEFT', 'KEY_RIGHT'):
                self.deck.flip_card()
            elif c in ('=', '+'):
                self.deck.increment_current_card()
            elif c in ('-', '_'):
                self.deck.decrement_current_card()
            elif c in ('1', '2'):
                self.deck.group_by(int(c))
            elif c in ('>', '.'):
                self.deck.move_sideboard()
            elif c in ('<', ','):
                self.deck.move_maindeck()
            elif c in ('~', '`'):
                self.deck.jump()
            elif c in ('/', '?', ';', ':', '\t'):
                break
            self.win.refresh()
        curses.curs_set(True)
        return c
