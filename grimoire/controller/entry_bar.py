from collections import deque
from curses.textpad import Textbox
from typing import Deque, Optional
from ..view.type_stubs import _CursesWindow
import curses
import curses.ascii
import logging
logger = logging.getLogger(__name__)


class EscapeException(Exception):
    pass


class EntryBar(object):
    KEY_RIGHT = 261
    KEY_LEFT = 260
    KEY_UP = 259
    KEY_DOWN = 258

    def __init__(self, screen: _CursesWindow, prompt: str, y: int, x: int, width: int) -> None:
        self.prompt = prompt
        screen.addstr(y, x, prompt)
        width -= len(prompt)
        self.entry = curses.newwin(1, width, y, x + len(prompt))
        self.bar = Textbox(self.entry, insert_mode=True)
        self.history: Deque[str] = deque()

    def edit(self) -> str:
        self._idx: Optional[int] = None
        self._escaped = False
        string = self.bar.edit(self._validate).strip()
        if self._escaped:
            raise EscapeException
        self._add_to_history(string)
        return string

    def _add_to_history(self, query: str) -> None:
        if not query or (self.history and query == self.history[0]):
            return
        self.history.appendleft(query)

    def _validate(self, key: int) -> int:
        '''adjust the key being typed'''
        y, x = self.entry.getyx()
        logger.debug('{}({}, {}) key press "{}" == {}'.format(self.prompt, y, x, chr(key), key))
        if key == curses.ascii.NL:
            return curses.ascii.BEL
        if key == curses.ascii.ESC:
            self._escaped = True
            return curses.ascii.BEL
        if key == curses.ascii.BS and self.entry.getyx() == (0, 0):
            # stop deleting characters under the cursor when we get to the start of the line
            return self.KEY_LEFT
        if key == self.KEY_RIGHT:
            # prevent the cursor from moving right if nothing is there
            strlen = len(self.bar.gather())
            if strlen == 0:
                return self.KEY_LEFT
            elif strlen == x + 1:
                self.entry.move(y, x - 1)
            else:
                # gather() automatically moves the cursor to the end, so reset it
                self.entry.move(y, x)
            return key
        if self.KEY_DOWN <= key <= self.KEY_UP:
            self._goto_history(key)
        return key

    def _goto_history(self, key: int) -> None:
        # implement in subclass if you want access to history buffer
        return
