from .entry_bar import EntryBar


class SearchBar(EntryBar):
    def edit(self) -> str:
        return super().edit().lower()

    def _goto_history(self, key: int) -> None:
        if not self.history:
            return

        if self._idx is None:
            self._idx = 0

        if key == self.KEY_UP:
            self._idx += 1 if self._idx < len(self.history) - 1 else 0
        elif key == self.KEY_DOWN:
            self._idx -= 1 if self._idx > 0 else 0
        self.entry.erase()
        self.entry.addstr(0, 0, self.history[self._idx])
