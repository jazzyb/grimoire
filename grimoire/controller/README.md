# Grimoire User Input

When you first start the `grimoire` program, you will be presented with the
following interface:

```
+---------------------------------------+
|Search:                                |
+--------++-------------++--------------+
|        ||             ||              |
|  (A)   ||     (C)     ||     (D)      |
|        ||             ||              |
+--------+|             ||              |
+--------+|             ||              |
|        ||             ||              |
|  (B)   ||             ||              |
|        ||             ||              |
+--------++-------------++--------------+
|run:               |                   |
+---------------------------------------+
```

The default panels are:
* Top:  [the Search Bar](#the-search-bar)
* (A):  the Card Panel
* (B):  the Scope Panel
* (C):  [the Results Panel](#the-results-panel)
* (D):  [the Deck Panel](#the deck panel)
* Bottom: [the Run Bar](#the-run-bar)

Please see the [Grimoire Visual Interface guide](../view/README.md) for
details about the purposes of each of these panels.  NOTE:  This document will
only focus on the commands used to navigate the interface.

## The Search Bar

The blinking cursor in this bar is where you will begin the program.  The
user may type a [Grimoire search query](../search/README.md) into the bar.
Upon hitting enter, the middle panel -- [the results
panel](#the-results-panel) -- will be filled with the results from the search,
and the focus will shift to that panel.

Hitting the up or down arrows will cycle through the history of queries.

Hitting `ESC` at any time in the search bar will return focus to the middle
panel **without** executing any search.  You may do this if you want to
return to the results list or deck without losing your place.

## The Results Panel

The results panel lists the cards returned from each search.  Navigation up
and down the list is possible with the up and down arrow keys (or `k` and
`j`), respectively.  The keys `g` and `G` will jump to the top or bottom of
the list, respectively.

The scope panel in the bottom left will show the various ways this list can be
ordered:
* by hitting `1`: order by Name (the default)
* by hitting `2`: order by Color
* by hitting `3`: order by Mana Value (CMC)

The names of dual-faced cards are preceded by the symbol `[>]` or `[<]`.
These symbols denote either the front or the back of a Modal DFC,
respectively.  You may temporarily change which side of the card is listed by
hitting the right or left arrow keys (or `l` and `h`).  When you do, the
information in the card panel will change appropriately.

Hitting `+` (or `=`) over a highlighted card in the results list will add that
card to the deck panel.  If the card already exists, the number will be
incremented.  Hitting `-` (or `_`) will decrement the number of cards in the
deck.  Reducing the number to less than one will remove that card from the
deck.

Hitting `TAB` will switch the focus to [the deck panel](#the-deck-panel).
This will swap the result list with the deck view, so that the deck will now
be in the middle panel, and the results will be on the right.  (The panel with
focus is always in the middle.)

Hitting `/` (or `?`) will return you to [the search bar](#the-search-bar).
Hitting `:` (or `;`) will drop you to [the run bar](#the-run-bar).

## The Deck Panel

The deck panel lists the cards that have been added to the deck.  The scope
panel shows how these cards can be organized:
* by hitting `1`: group by Mana Value (the default)
* by hitting `2`: group by Card Type

Navigation up and down the list of cards is done with the up and down arrow
keys (or `k` and `j`), respectively.  When you do, the information in the card
panel will change appropriately.

As in the results panel, dual-faced cards are indicated by `[>]` or `[<]`, and
the right and left keys (or `l` and `h`) will temporarily change which side of
the card is listed.

Hitting `+` or `-` (also `=` or `_`) increments or decrements the number of
the selected card in the deck.

Cards may be added to the sideboard, at the bottom of the panel, by hitting
`>` (or `.`) on the highlighted card.  Hitting `<` (or `,`) will move a card
from the sideboard to the mainboard.

The user may jump between the maindeck and sideboard by hitting the `~` or
`` ` `` keys.

Hitting `TAB` will switch focus back to [the results
panel](#the-results-panel).

Hitting `/` (or `?`) will return you to [the search bar](#the-search-bar).
Hitting `:` (or `;`) will drop you to [the run bar](#the-run-bar).

## The Run Bar

### Commands

The user can execute the following commands at the run bar:

* `calc`
  - hypergeometric calculator (see [below](#hypergeometric-calculator))
* `export`
  - will copy the current deck (saved or unsaved) to the user's clipboard.
* `import [-s] [-d]`
  - will replace the current deck with the contents of the clipboard.  The
    `-s` options will save the current deck before replacing it, and `-d` will
    discard any changes.  If there are unsaved changes in the deck, and
    neither option is provided, grimoire will prompt the user to cancel the
    command, save the deck, or discard the changes first.
* `new [-s] [-d] [<deck_file>]`
  - will create a new *empty* deck with the name `<deck_file>`.  If the user
    specifies no filename, then the default `untitled.dec` will be used.  The
    `-s` and `-d` options work like `import` above.  **`<deck_file>` cannot
    have spaces in it.**
* `open [-s] [-d] <deck_file>`
  - will replace the current deck with the contents of `<deck_file>`.  The
    `-s` and `-d` options work like `import` above.  **`<deck_file>` cannot
    have spaces in it.**
* `save [<deck_file>]`
  - will save the current deck to `<deck_file>`.  If the user specifies no
    filename, then the current filename will be used (`untitled.dec` by
    default).  **`<deck_file>` cannot have spaces in it.**
* `quit [-s] [-d]`
  - will exit grimoire.  The `-s` and `-d` options work like `import` above.
* `wq`
  - shortcut for `quit -s`
* `q!`
  - shortcut for `quit -d`

Hitting the up or down arrows will cycle through the history of commands.

Like the search bar, hitting `ESC` at any time in the run bar will return
focus to the middle panel **without** executing any command.  You may do this
if you want to cancel the current command.

### Tab Completion

All of the commands (except `q!`) can be tab-completed.  So that, e.g.,
`o<TAB>` will become `open `.  For commands which take a `<deck_file>`, the
filename can also be tab-completed.  For example, say the user has a deck
called `decks/net.dec` on their filesystem.  Typing `open decks/n<TAB>` will
complete to `open decks/net.dec`.  If multiple filenames match, then hitting
`<TAB>` repeatedly will cycle through the matching files.

### Hypergeometric Calculator

The `calc` command implements a hypergeometric calculator.  Given a number of
success states `K` in a population `N`, a hypergeometric calculator determines
the probability of seeing `k` of those success states in a sample size of `n`.

Hypergeometric calculators are often used in deck-building to create a solid
mana-base.  For example, say you have 24 lands in a 60-card deck, and you want
to know how often you will see *at least* 2 lands in your opening hand of
seven cards:

    calc -N 60 -K 24 -n 7 -k 2

This produces the result 85.7%.  The `N` option if omitted defaults to 60, so
the above command could also be written:

    calc -K 24 -n 7 -k 2

Or instead of stating `n` explicitly, we could talk in terms of "turns":

    calc -K 24 -t 1 -k 2

This defaults to being on the "play".  If you are on the "draw" and want to
know your odds of hitting 2 lands in your opening hand plus the first draw
(i.e. first 8 cards), then you can calculate:

    calc -K 24 -t 1 -k 2 --on-draw

which gives you 91.0%.

You can use the `calc` command to determine, for example, how many black mana
sources to include in your multicolor deck in order to see two such lands by
turn 5 on the play:

    calc -t 5 -k 2 -K <number_of_black_mana_sources>

And then adjust `K` until the probability is acceptably high for your purposes.
