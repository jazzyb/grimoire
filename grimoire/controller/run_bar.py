from argparse import ArgumentParser, Namespace
from .entry_bar import EntryBar, EscapeException
from ..error import GrimoireError
from typing import List, NoReturn, Optional
from ..view.type_stubs import _CursesWindow
import curses.ascii
import logging
import os
import re
logger = logging.getLogger(__name__)


class CommandError(GrimoireError):
    pass


class CommandParser(ArgumentParser):
    def error(self, message: str) -> NoReturn:
        logger.error(message)
        # shorten the message
        raise CommandError(re.sub(r' \(choose from .*$', '', message))


class RunBar(EntryBar):
    COMMANDS = ['calc', 'export', 'import', 'new', 'open', 'save', 'wq', 'quit']

    def __init__(self, screen: _CursesWindow, prompt: str, y: int, x: int, width: int) -> None:
        super().__init__(screen, prompt, y, x, width)
        self.parser = CommandParser(prog=prompt.strip())
        subparsers = self.parser.add_subparsers(dest='command')

        # calculate hypergeometric distributions
        calc_cmd = subparsers.add_parser('calc')
        calc_cmd.add_argument('-N', type=int, default=60, help='number of cards in the deck')
        calc_cmd.add_argument('-K', type=int, required=True,
                              help='number of "successes" in the deck')
        calc_cmd.add_argument('-n', type=int, help='number of cards seen')
        calc_cmd.add_argument('-k', type=int, required=True,
                              help='number of "successes" among the cards seen')
        calc_cmd.add_argument('-t', type=int, help='number of turns')
        calc_cmd.add_argument('--on-play', dest='on_play', action='store_true')
        calc_cmd.add_argument('--on-draw', dest='on_play', action='store_false')
        calc_cmd.set_defaults(on_play=True)

        # export/import to/from clipboard
        subparsers.add_parser('export')
        import_cmd = subparsers.add_parser('import')
        self._add_save_option(import_cmd)

        # new [filename.dec]
        new_cmd = subparsers.add_parser('new')
        self._add_save_option(new_cmd)
        new_cmd.add_argument('deck_file', nargs='?', help='new deck filename')

        # open <filename.dec>
        open_cmd = subparsers.add_parser('open')
        self._add_save_option(open_cmd)
        open_cmd.add_argument('deck_file', nargs=1, help='path to deck list')

        # save [filename.dec]
        save_cmd = subparsers.add_parser('save')
        save_cmd.add_argument('deck_file', nargs='?', help='path to deck list')

        # quit
        quit_cmd = subparsers.add_parser('quit')
        self._add_save_option(quit_cmd)
        subparsers.add_parser('wq')  # save current deck and quit
        subparsers.add_parser('q!')  # discard current deck and quit

    def parse(self, deck_saved: bool = True) -> Namespace:
        self.options: List[str] = []
        cmd = super().edit()
        logger.info('run: "{}"'.format(cmd))
        self.entry.erase()
        if not cmd or self._escaped:
            raise EscapeException
        return self._parse_args(cmd, deck_saved)

    # PRIVATE METHODS

    def _add_save_option(self, cmd: ArgumentParser) -> None:
        cmd.add_argument('-s', '--save', dest='save', action='store_true')
        cmd.add_argument('-d', '--discard', dest='save', action='store_false')
        cmd.set_defaults(save=None)

    def _parse_args(self, cmd: str, deck_saved: bool) -> Namespace:
        must_save_cmds = ('import', 'new', 'open', 'quit')
        args = self.parser.parse_args(cmd.split())
        if args.command in must_save_cmds and args.save is None and not deck_saved:
            args.save = self._prompt_to_save(cmd)
        if args.command == 'calc' and args.n is None and args.t is None:
            raise CommandError('either cards seen (-n) or turns (-t) must be provided')
        return args

    def _prompt_to_save(self, orig_cmd: str) -> bool:
        self._overwrite_bar('Save changes to deck? ([C]ancel, [S]ave, or [D]iscard)')
        self._answer: str = 'cancel'
        self.bar.edit(self._save_or_discard)
        self._overwrite_bar(orig_cmd)
        if self._answer == 'cancel':
            raise EscapeException
        self.entry.erase()
        if self._answer == 'save':
            return True
        else:
            assert self._answer == 'discard'
            return False

    def _overwrite_bar(self, string: str) -> None:
        self.entry.erase()
        self.entry.addstr(0, 0, string)
        self.entry.refresh()

    def _save_or_discard(self, key: int) -> int:
        if key in (ord('C'), ord('c')):
            self._answer = 'cancel'
            return curses.ascii.BEL
        elif key in (ord('S'), ord('s')):
            self._answer = 'save'
            return curses.ascii.BEL
        elif key in (ord('D'), ord('d')):
            self._answer = 'discard'
            return curses.ascii.BEL
        return key

    def _validate(self, key: int) -> int:
        if key == curses.ascii.TAB:
            cmd = self._complete_command()
            if cmd is None:
                cmd = self._complete_filename()
            if cmd is not None:
                self.entry.erase()
                self.entry.addstr(0, 0, cmd)
        else:
            self.options = []
        return super()._validate(key)

    def _complete_command(self) -> Optional[str]:
        string = self.bar.gather().strip().lower()
        for cmd in self.COMMANDS:
            if cmd.startswith(string):
                return cmd + ' '
        return None

    def _complete_filename(self) -> Optional[str]:
        string = self.bar.gather().strip()
        if string.lower().startswith('open ') or string.lower().startswith('save '):
            if not self.options:
                self.options = self._find_matching_files(string)
            if self.options:
                new_cmd = self.options.pop()
                return new_cmd
        return None

    def _find_matching_files(self, string: str) -> List[str]:
        logger.debug('looking for matching filenames')
        files: List[str] = []
        # split the string into ["command_plus_options", "possible_filename"]
        words = [x[::-1] for x in string[::-1].split(maxsplit=1)][::-1]
        cmd, partial = words if len(words) >= 2 else (words[0], '')

        dirpath, rest = os.path.split(partial)
        logger.debug('path == "{}" + "{}"'.format(dirpath, rest))
        for filename in sorted(os.listdir(os.path.join('.', dirpath)), reverse=True):
            if filename.startswith(rest):
                filename = os.path.join(dirpath, filename)
                logger.debug('found matching path: "{}"'.format(filename))
                if os.path.isdir(filename):
                    filename += os.sep
                files.append(' '.join((cmd, filename)))
        return [string] + files

    def _goto_history(self, key: int) -> None:
        if not self.history:
            return

        if key == self.KEY_UP:
            if self._idx is None:
                self._idx = -1
            self._idx += 1 if self._idx < len(self.history) - 1 else 0
        elif key == self.KEY_DOWN:
            if self._idx is None:
                return
            self._idx -= 1 if self._idx > 0 else 0
        else:
            return
        self.entry.erase()
        self.entry.addstr(0, 0, self.history[self._idx])
