from .base_controller import BaseController
from ..model.result_model import ResultModel
from ..view.window import InterfaceWindow
import curses
import logging
logger = logging.getLogger(__name__)


def log_key_press(key: str) -> None:
    if len(key) == 1:
        logger.debug('key press: "{}" == {}'.format(key, ord(key)))
    else:
        logger.debug('key press: "{}"'.format(key))


class ResultController(BaseController):
    def __init__(self, window: InterfaceWindow, model: ResultModel) -> None:
        self.result = model
        super().__init__(window)

    def control(self) -> str:
        self.result.update_subordinate_panels()
        self.win.refresh()
        curses.curs_set(False)
        while True:
            c = self.win.getkey()
            log_key_press(c)
            if c in ('j', 'J', 'KEY_DOWN'):
                self.result.move_down()
            elif c in ('k', 'K', 'KEY_UP'):
                self.result.move_up()
            elif c in ('h', 'H', 'l', 'L', 'KEY_LEFT', 'KEY_RIGHT'):
                self.result.flip_card()
            elif c == 'g':
                self.result.goto_top()
            elif c == 'G':
                self.result.goto_bottom()
            elif c in ('=', '+'):
                self.result.increment_current_card()
            elif c in ('-', '_'):
                self.result.decrement_current_card()
            elif c in ('1', '2', '3'):
                self.result.order_by(int(c))
            elif c in ('/', '?', ';', ':', '\t'):
                break
            self.win.refresh()
        curses.curs_set(True)
        return c
