from ..view.window import InterfaceWindow


class BaseController(object):
    def __init__(self, window: InterfaceWindow) -> None:
        self.win = window

    def control(self) -> str:
        raise NotImplementedError
