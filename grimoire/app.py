from .controller.base_controller import BaseController
from .controller.deck_controller import DeckController
from .controller.entry_bar import EscapeException
from .controller.result_controller import ResultController
from .data.database import Database
from enum import IntEnum, unique
from .error import GrimoireError
from .model.deck_model import DeckModel, ExitProgramException
from .model.result_model import ResultModel
from typing import List, Optional
from .view.type_stubs import _CursesWindow
from .view.window import InterfaceWindow
import sys
import logging
logger = logging.getLogger(__name__)


@unique
class Ctrl(IntEnum):
    RESULTS = 0
    DECK = 1


class GrimoireApp(object):
    """The main Grimoire app"""

    def __init__(self, screen: _CursesWindow, dbfile: str, deck: Optional[str] = None) -> None:
        self.deck_filename = deck

        # views:
        self.win = InterfaceWindow(screen)

        # models:
        self.db = Database(dbfile)
        self.deck = DeckModel(self.db, self.win.deck_view, self.win.card_face_view,
                              self.win.scope_view)
        self.results = ResultModel(self.db, self.win.result_view,
                                   self.win.card_face_view, self.win.scope_view)
        self.deck.results = self.results
        self.results.deck = self.deck

        # controllers:
        self.controllers: List[BaseController] = []
        self.controllers.append(ResultController(self.win, self.results))
        self.controllers.append(DeckController(self.win, self.deck))

    def run(self) -> None:
        try:
            self._run()
        except:  # noqa: E722
            # if we run into any uncaught exception (a bug), then save the
            # current deck
            logger.exception('Uncaught exception !!!')
            if not self.deck.is_saved():
                self.deck.write(prefix = 'SAVED-')
                print('Deck saved\n', file=sys.stderr)
            raise

    def _run(self) -> None:
        self.idx = Ctrl.RESULTS
        self.win.refresh()

        if self.deck_filename is not None:
            self.deck.open(self.deck_filename)
            self._swap_panels()
        else:
            self._search()

        while True:
            c = self.controllers[self.idx].control()
            if c in ('/', '?'):
                self._search()
            elif c in (';', ':'):
                try:
                    self._execute()
                except ExitProgramException:
                    break
            elif c == '\t':
                self._swap_panels()

    def _search(self) -> None:
        try:
            query = self.win.search()
        except EscapeException:
            pass
        else:
            if self.idx != Ctrl.RESULTS:
                self.idx = Ctrl.RESULTS
                self.win.swap_panels()
            try:
                msg = self.results.update(query)
            except GrimoireError as e:
                self.win.error(e.message)
            else:
                self.win.info(msg)

    def _execute(self) -> None:
        try:
            cmd = self.win.execute(self.deck.is_saved())
        except GrimoireError as e:
            self.win.error(e.message)
        except EscapeException:
            pass
        else:
            try:
                msg = self.deck.process(cmd)
            except GrimoireError as e:
                self.win.error(e.message)
            else:
                self.win.info(msg)
                if 'opened' in msg and self.idx != Ctrl.DECK:
                    self.idx = Ctrl.DECK
                    self.win.swap_panels()

    def _swap_panels(self) -> None:
        self.idx = Ctrl((self.idx + 1) % len(Ctrl))
        self.win.swap_panels()
