from ..data.card import Card
from typing import Optional, Tuple


class ModelCard(object):
    """Wrapper object around the database Cards"""

    def __init__(self, card: Card, back: Optional['ModelCard'] = None, count: int = 0) -> None:
        self.card = card
        self._back = back
        self._md_count = count
        self._sb_count = 0

    def key(self) -> Tuple[str, bool]:
        """unique identifier"""
        return (self.card.scryfall_id, self.card.is_back)

    def back_key(self) -> Tuple[str, bool]:
        return (self.card.scryfall_id, not self.card.is_back)

    def is_modal_dfc(self) -> bool:
        return self.card.layout == 'modal_dfc'

    def is_transform(self) -> bool:
        return self.card.layout == 'transform'

    def is_flip_card(self) -> bool:
        return self.card.layout in ('modal_dfc', 'transform')

    def is_back(self) -> bool:
        return self.card.is_back

    def invert(self) -> 'ModelCard':
        """return a card with front and back swapped"""
        if self.is_modal_dfc():
            card = ModelCard(self.back.card, ModelCard(self.card))
            card._md_count = self._md_count
            card._sb_count = self._sb_count
            card._update_back_counts()
            return card
        return self

    def maindeck(self, num: int = 1) -> None:
        """move num cards from sideboard to maindeck"""
        if self._sb_count < num:
            return
        self._sb_count -= num
        self._md_count += num
        self._update_back_counts()

    def sideboard(self, num: int = 1) -> None:
        """move num cards from maindeck to sideboard"""
        if self._md_count < num:
            return
        self._md_count -= num
        self._sb_count += num
        self._update_back_counts()

    @property
    def back(self) -> 'ModelCard':
        assert self._back is not None
        return self._back

    @back.setter
    def back(self, back: 'ModelCard') -> None:
        self._back = back

    @property
    def md_count(self) -> int:
        """number of cards in the maindeck"""
        return self._md_count

    @property
    def sb_count(self) -> int:
        """number of cards in the sideboard"""
        return self._sb_count

    @property
    def count(self) -> int:
        """total number of cards in the deck"""
        return self._md_count + self._sb_count

    @count.setter
    def count(self, new_cnt: int) -> None:
        """change the count of this card in the deck"""
        if new_cnt >= self.count:
            # always add cards to the maindeck
            self._md_count += new_cnt - self.count
        else:
            # remove cards from maindeck first
            self._md_count -= self.count - new_cnt
            if self._md_count < 0:
                self._sb_count += self._md_count
                self._md_count = 0
        self._update_back_counts()

    @property
    def id(self) -> str:
        return self.card.scryfall_id

    @property
    def name(self) -> str:
        return self.card.name

    def display_name(self) -> str:
        if self.is_modal_dfc():
            return '[{}] '.format('<' if self.card.is_back else '>') + self.card.name
        elif self.is_transform():
            return '[{}] '.format('☾' if self.card.is_back else '☉') + self.card.name
        else:
            return self.card.name

    @property
    def mana(self) -> str:
        return self.card.mana

    @property
    def cmc(self) -> int:
        return self.card.cmc

    @property
    def color_w(self) -> bool:
        return self.card.color_w

    @property
    def color_u(self) -> bool:
        return self.card.color_u

    @property
    def color_b(self) -> bool:
        return self.card.color_b

    @property
    def color_r(self) -> bool:
        return self.card.color_r

    @property
    def color_g(self) -> bool:
        return self.card.color_g

    @property
    def types(self) -> str:
        return self.card.types

    def _update_back_counts(self) -> None:
        if self._back is not None:
            self._back._md_count = self._md_count
            self._back._sb_count = self._sb_count
