from collections import defaultdict
from ..data.card import Card
from ..data.database import Database
from enum import Enum
from .model_card import ModelCard
from ..search import where_clause
from ..search.lexer import tokenize
from ..search.parser import parse
from .type_stubs import DeckModel
from typing import Dict, List, Optional, Sequence, Tuple
from ..view.card_face_view import CardFaceView
from ..view.display_listing import DisplayListing
from ..view.result_view import ResultView
from ..view.scope_view import ScopeView
import logging
logger = logging.getLogger(__name__)


class Order(Enum):
    NAME = 1
    COLOR = 2
    CMC = 3


class ResultModel(object):
    def __init__(self, db: Database, view: ResultView, face: CardFaceView, scope: ScopeView,
                 deck: Optional[DeckModel] = None) -> None:
        self.cards: Sequence[ModelCard] = []
        self.lookup_table: Dict[Tuple[str, bool], ModelCard] = {}
        self.idx = 0
        self.is_flipped = False
        self.db = db
        self.deck = deck
        self.view = view
        self.face = face
        self.scope = scope

    @property
    def deck(self) -> Optional[DeckModel]:
        return self._deck

    @deck.setter
    def deck(self, deck: Optional[DeckModel]) -> None:
        self._deck = deck
        self.card_counts: Dict[str, int] = defaultdict(int)

    @property
    def view(self) -> ResultView:
        return self._view

    @view.setter
    def view(self, view: ResultView) -> None:
        self._view = view
        self.title = view.DEFAULT_TITLE

    @property
    def scope(self) -> ScopeView:
        return self._scope

    @scope.setter
    def scope(self, scope: ScopeView) -> None:
        self.sort_order = Order.NAME
        self._scope = scope
        self._update_scope()

    def update(self, query: str) -> str:
        """run query on the database and update views with cards"""
        logger.info('query: "{}"'.format(query))
        self.cards = self._fetch_cards(query)
        self.idx = 0
        self._set_title()
        self.view.update_data(self._display_cards(self.cards))
        self._update_face(self.current)
        return ''

    @property
    def current(self) -> Optional[ModelCard]:
        """the current card"""
        return self.cards[self.idx] if self.cards else None

    def move_up(self) -> None:
        """scroll up"""
        if self.idx <= 0:
            return
        self.idx -= 1
        self.view.move_up()
        self._update_face(self.current)

    def move_down(self) -> None:
        """scroll down"""
        if self.idx >= len(self.cards) - 1:
            return
        self.idx += 1
        self.view.move_down()
        self._update_face(self.current)

    def flip_card(self) -> None:
        """flip the current card if it is an MDFC"""
        if self.current is None or not self.current.is_flip_card():
            return
        if self.is_flipped:
            self._update_face(self.current)
            self.view.temp_replace(self._display_card(self.current))
        else:
            self.is_flipped = True
            self.face.update(self.current.back.card, self.current.card)
            self.view.temp_replace(self._display_card(self.current.back))

    def goto_top(self) -> None:
        """jump to the first card"""
        self.idx = 0
        self.view.goto_top()
        self._update_face(self.current)

    def goto_bottom(self) -> None:
        """jump to the last card"""
        self.idx = len(self.cards) - 1
        self.view.goto_bottom()
        self._update_face(self.current)

    def increment_current_card(self) -> None:
        """add the current card to the deck"""
        if self.current is None or self.deck is None:
            return
        if self.is_flipped:
            count = self.deck.add(self.current.invert())
        else:
            count = self.deck.add(self.current)
        self._update_card_count(self.current, count)

    def decrement_current_card(self) -> None:
        """remove the current card from the deck"""
        if self.current is None or self.current.count == 0 or self.deck is None:
            return
        count = self.deck.remove(self.current)
        self._update_card_count(self.current, count)

    def set_card_count(self, card: ModelCard, count: Optional[int] = None) -> None:
        """set the count of card"""
        if count is None:
            count = card.count
        self._update_card_count(card, count, self._card_index(card))

    def order_by(self, cmd: int) -> None:
        """set how the cards are ordered"""
        order_by = Order(cmd)
        if order_by == self.sort_order:
            return
        self.sort_order = order_by
        self.idx = 0
        self.cards = self._sort_cards(self.cards)
        self.view.update_data(self._display_cards(self.cards))
        self._update_face(self.current)
        self.scope.select(self.sort_order.value)

    def update_subordinate_panels(self) -> None:
        self._update_face(self.current)
        self._update_scope()

    # PRIVATE METHODS

    def _set_title(self) -> None:
        if not self.cards:
            self.title = 'NO Results'
        elif len(self.cards) == 1:
            self.title = '1 Result'
        else:
            self.title = '{} Results'.format(len(self.cards))
        self.view.title = self.title

    def _fetch_cards(self, query: str) -> Sequence[ModelCard]:
        ret: List[ModelCard] = []
        self.lookup_table = {}
        clause = where_clause(parse(tokenize(query.strip().lower())))
        cards = self.db.fetch(clause)
        for card in cards:
            if card.layout == 'transform' and card.is_back:
                # default to show the front of transform cards even if it's
                # the back that matches the query
                card = self._fetch_transform_front(card)
            rc = ModelCard(card, count=self.card_counts[card.scryfall_id])
            if rc.key() not in self.lookup_table:
                # ignore duplicates
                ret.append(rc)
                self.lookup_table[rc.key()] = rc
        self._fetch_flip_cards(ret)
        return self._sort_cards(ret)

    def _fetch_transform_front(self, card: Card) -> Card:
        query = "(scryfall_id = '{}' AND is_back = 0)".format(card.scryfall_id)
        return self.db.fetchone(query)

    def _fetch_flip_cards(self, cards: Sequence[ModelCard]) -> None:
        for card in cards:
            if not card.is_flip_card():
                continue
            if card.back_key() in self.lookup_table:
                card.back = self.lookup_table[card.back_key()]
                assert card.count == card.back.count
            else:
                query = "(scryfall_id = '{}' AND is_back = {})".format(*card.back_key())
                back = self.db.fetchone(query)
                card.back = ModelCard(back, card, card.count)

    def _update_card_count(self, card: ModelCard, count: int, idx: Optional[int] = None) -> None:
        self.card_counts[card.id] = count
        if self._in_results(card):
            self.lookup_table[card.key()].count = count
            self.view.replace(self._display_card(card), idx)
            if self.current and self.is_flipped and self.current.id == card.id:
                # if the current card is supposed to be flipped, then update
                # it, too
                self.view.temp_replace(self._display_card(card.back))
        if card.is_modal_dfc() and self._in_results(card.back):
            # if the back of the MDFC is in our list, we need to replace the
            # old info in the view
            self.lookup_table[card.back_key()].count = count
            idx = self._card_index(card.back)
            assert idx is not None
            self.view.replace(self._display_card(self.cards[idx]), idx)

    def _in_results(self, card: ModelCard) -> bool:
        """return whether or not card is in our results list"""
        return card.key() in self.lookup_table

    def _card_index(self, card: ModelCard) -> Optional[int]:
        for idx, item in enumerate(self.cards):
            if item.key() == card.key():
                return idx
        return None

    def _display_cards(self, cards: Sequence[ModelCard]) -> Sequence[DisplayListing]:
        return [self._display_card(x) for x in cards]

    def _display_card(self, card: ModelCard) -> DisplayListing:
        result = DisplayListing()
        count = card.count
        if count:
            result.left['number_bold'] = format(count, '>2')
        else:
            result.left['number'] = '  '
        result.left['name_bold' if count else 'name'] = card.display_name()
        result.right['mana'] = card.mana
        return result

    def _update_face(self, card: Optional[ModelCard] = None) -> None:
        self.is_flipped = False
        if card is None:
            self.face.update()
        elif card.is_flip_card():
            self.face.update(card.card, card.back.card)
        else:
            self.face.update(card.card)

    def _update_scope(self) -> None:
        self.scope.title = 'ORDER BY'
        listings: List[DisplayListing] = []
        for i, string in enumerate(['Name', 'Color', 'Mana Value (CMC)']):
            dl = DisplayListing()
            dl.left['cmd'] = str(i + 1)
            dl.left['order_by'] = string
            listings.append(dl)
        self.scope.fill(listings, self.sort_order.value)

    def _sort_cards(self, cards: Sequence[ModelCard]) -> Sequence[ModelCard]:
        key_func = {Order.NAME: lambda x: x.name,
                    Order.COLOR: self._sort_by_color,
                    Order.CMC: self._sort_by_cmc}[self.sort_order]
        return sorted(cards, key = key_func)

    def _sort_by_color(self, card: ModelCard) -> float:
        key = 0
        if card.color_w:
            key = key * 10 + 1
        if card.color_u:
            key = key * 10 + 2
        if card.color_b:
            key = key * 10 + 3
        if card.color_r:
            key = key * 10 + 4
        if card.color_g:
            key = key * 10 + 5
        if key == 0:
            key = 30000 if 'Land' in card.types else 20000
        return key + (card.cmc / 100)

    def _sort_by_cmc(self, card: ModelCard) -> float:
        key = 0
        if card.color_w:
            key = key * 10 + 1
        if card.color_u:
            key = key * 10 + 2
        if card.color_b:
            key = key * 10 + 3
        if card.color_r:
            key = key * 10 + 4
        if card.color_g:
            key = key * 10 + 5
        if key == 0:
            key = 20000
        return card.cmc + (key / 100000)
