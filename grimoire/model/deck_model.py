from argparse import Namespace
from .calculator import at_least_prob
from ..data.database import Database
from ..error import GrimoireError
from .focus import Focus
from .model_card import ModelCard
from .sorted_deck import Group, SortedDeck
from .type_stubs import ResultModel
from typing import Dict, List, Optional, Sequence, Tuple
from ..view.card_face_view import CardFaceView
from ..view.deck_view import DeckView
from ..view.display_listing import DisplayListing
from ..view.scope_view import ScopeView
import os
import pyperclip  # type: ignore  # FIXME
import logging
logger = logging.getLogger(__name__)


class ExitProgramException(Exception):
    pass


class DeckError(GrimoireError):
    pass


class DeckModel(object):
    DEFAULT_BASENAME = 'untitled'

    def __init__(self, db: Database, view: DeckView, face: CardFaceView, scope: ScopeView,
                 results: Optional[ResultModel] = None) -> None:
        self.deck = SortedDeck(Group.CMC)
        self.results = results
        self.db = db
        self.view = view
        self.face = face
        self.scope = scope

        self.filename = self._untitled_deck_name()
        self.is_flipped = False
        self.idx: Optional[int] = None
        self.latest: Optional[ModelCard] = None
        self.cards: Dict[str, ModelCard] = {}
        self.focus = Focus.MAIN

    @property
    def results(self) -> Optional[ResultModel]:
        return self._results

    @results.setter
    def results(self, results: Optional[ResultModel]) -> None:
        self._results = results

    @property
    def view(self) -> DeckView:
        return self._view

    @view.setter
    def view(self, view: DeckView) -> None:
        self._view = view

    @property
    def scope(self) -> ScopeView:
        return self._scope

    @scope.setter
    def scope(self, scope: ScopeView) -> None:
        self._scope = scope
        self._update_scope()

    @property
    def current(self) -> Optional[ModelCard]:
        if self.idx is None:
            return None
        try:
            return self.deck[self.idx] if self.is_main() else self.deck.sideboard[self.idx]
        except IndexError:
            return None

    @property
    def filename(self) -> str:
        return self._filename

    @filename.setter
    def filename(self, newfn: str) -> None:
        self._filename = newfn
        self._update_title()

    @property
    def maindeck(self) -> Sequence[ModelCard]:
        """all cards in the maindeck"""
        return [x for x in self.cards.values() if x.md_count]

    @property
    def sideboard(self) -> Sequence[ModelCard]:
        """all cards in the sideboard"""
        return [x for x in self.cards.values() if x.sb_count]

    def open(self, filename: str) -> None:
        """open the given deck"""
        deck, side = self._import_file(filename)
        self._replace_deck(deck, side, filename)

    def write(self, prefix: str = '') -> None:
        """save the current deck file"""
        filename = self.filename if self.is_saved() else self.filename[1:]
        dirname, basename = os.path.split(filename)
        self._save('save', os.path.join(dirname, prefix + basename))

    def process(self, args: Namespace) -> str:
        """execute the given command"""
        if args.command in ('quit', 'q!', 'wq'):
            if args.command == 'wq' or (args.command == 'quit' and args.save):
                self._save(args.command)
            raise ExitProgramException

        elif args.command == 'save':
            self._save(args.command, args.deck_file)
            return 'saved "{}"'.format(self.filename)

        elif args.command == 'open':
            if args.save:
                self._save(args.command)
            filename = args.deck_file[0]
            self.open(filename)
            return 'opened "{}"'.format(self.filename)

        elif args.command == 'import':
            if args.save:
                self._save(args.command)
            deck, side = self._import_deck(pyperclip.paste())
            self._replace_deck(deck, side)
            return 'imported deck'

        elif args.command == 'export':
            pyperclip.copy(self._export_all(fronts_only = True))
            return 'exported deck'

        elif args.command == 'new':
            if args.save:
                self._save(args.command)
            name = args.deck_file if args.deck_file else self._untitled_deck_name()
            if os.path.exists(name):
                raise DeckError('error creating deck, "{}" already exists'.format(name))
            self._replace_deck([], [], name)
            return 'created "{}"'.format(name)

        elif args.command == 'calc':
            if args.n is None:
                n = args.t + 6 + (0 if args.on_play else 1)
            else:
                n = args.n
            # Given K specific cards in an N card deck, calculate the
            # probability that k of those cards are in the first n cards seen:
            return '%0.1f%% ' % (at_least_prob(args.N, args.K, n, args.k) * 100,)

        # should never reach here
        assert False
        return ''

    def increment_current_card(self) -> None:
        """increment the current/latest card

        Called from controller
        """
        if self.latest is None:
            return
        self.latest.count += 1
        if not self.is_main():
            self.latest.sideboard(1)
        self._log_card_change('inc', self.latest, self.latest.count)
        if self.results is not None:
            self.results.set_card_count(self.latest)
        self.view.update(self._display_deck(), self.idx, self._display_sb())
        self._mark_deck_as_modified()

    def decrement_current_card(self) -> None:
        """decrement the current/latest card

        Called from controller
        """
        if self.latest is None:
            return
        self.latest.count -= 1
        self._log_card_change('dec', self.latest, self.latest.count)
        if self.results is not None:
            self.results.set_card_count(self.latest)
        if self.latest.count == 0:
            self._remove_card(self.latest)
            if self.idx == len(self.maindeck if self.is_main() else self.sideboard):
                self.idx -= 1
                if self.idx < 0:
                    self.idx = None
            self.latest = self.current
            self._update_face(self.latest)
        elif not self.is_main():
            self.latest.sideboard(1)
            if self.idx == len(self.sideboard):
                self.idx -= 1
                if self.idx < 0:
                    self.idx = None
            self.latest = self.current
            self._update_face(self.latest)
        self.view.update(self._display_deck(), self.idx, self._display_sb())
        self._mark_deck_as_modified()

    def add(self, card: ModelCard) -> int:
        """add a card to the deck and return how many are in the deck"""
        if card.id in self.cards:
            self.latest = self.cards[card.id]
            self.latest.count += 1
        else:
            card.count = 1
            self.cards[card.id] = self.latest = card
            self.deck.add(self.latest)
        self._log_card_change('add', card, self.latest.count)
        self.idx = self._find_latest_index()
        if not self.is_main():
            self.focus = Focus.MAIN
            self.view.set_focus(self.focus, self.idx)
        self.view.update(self._display_deck(), self.idx, self._display_sb())
        self._mark_deck_as_modified()
        return self.latest.count

    def remove(self, card: ModelCard) -> int:
        """remove one of the given card in the deck and return the new count"""
        if card.id not in self.cards:
            return 0
        self.latest = self.cards[card.id]
        self.latest.count -= 1
        self._log_card_change('rem', card, self.latest.count)
        if self.latest.count == 0:
            self._remove_card(card)
        self.idx = self._find_latest_index()
        if not self.is_main():
            self.focus = Focus.MAIN
            self.view.set_focus(self.focus, self.idx)
        self.view.update(self._display_deck(), self.idx, self._display_sb())
        self._mark_deck_as_modified()
        return self.latest.count

    def move_up(self) -> None:
        """scroll up"""
        if self.idx is None:
            self.idx = 0
        if self.idx <= 0:
            return
        self.idx -= 1
        self.latest = self.current
        self._update_face(self.latest)
        self.view.update_indices(self.idx, [self.idx, self.idx + 1])

    def move_down(self) -> None:
        """scroll down"""
        if self.idx is None:
            self.idx = 0
        if self.idx >= len(self.maindeck if self.is_main() else self.sideboard) - 1:
            return
        self.idx += 1
        self.latest = self.current
        self._update_face(self.latest)
        self.view.update_indices(self.idx, [self.idx - 1, self.idx])

    def flip_card(self) -> None:
        """flip the current card if it is an MDFC"""
        if self.latest is None:
            self.latest = self.current
        if self.latest is None or not self.latest.is_flip_card():
            return
        if self.is_flipped:
            self._update_face(self.latest)
            self.view.temp_replace(self._display_card(self.latest))
        else:
            self.is_flipped = True
            self.face.update(self.latest.back.card, self.latest.card)
            self.view.temp_replace(self._display_card(self.latest.back))

    def move_maindeck(self) -> None:
        """move current card from sideboard to maindeck"""
        if self.latest is None:
            return
        self.latest.maindeck(1)
        self.view.update(self._display_deck(), self.idx, self._display_sb())
        self._mark_deck_as_modified()

    def move_sideboard(self) -> None:
        """move current card from maindeck to sideboard"""
        if self.latest is None:
            return
        self.latest.sideboard(1)
        if self.latest.md_count == 0:
            if self.idx == len(self.maindeck):
                self.idx -= 1
                if self.idx < 0:
                    self.idx = None
            self.latest = self.current
            self._update_face(self.latest)
        self.view.update(self._display_deck(), self.idx, self._display_sb())
        self._mark_deck_as_modified()

    def jump(self) -> None:
        """switch to sideboard -- or if in sideboard, switch to maindeck"""
        if self.focus == Focus.MAIN:
            self.focus = Focus.SIDE
            self.idx = 0 if len(self.sideboard) else None
        else:
            assert self.focus == Focus.SIDE
            self.focus = Focus.MAIN
            self.idx = 0 if len(self.maindeck) else None
        self.latest = self.current
        self._update_face(self.latest)
        self.view.set_focus(self.focus, self.idx)

    def group_by(self, cmd: int) -> None:
        group_order = Group(cmd)
        if group_order == self.deck.group_order:
            return
        self.deck.group_order = group_order
        self.idx = 0
        self.latest = self.current
        self.view.update(self._display_deck(), self.idx, self._display_sb())
        self._update_face(self.latest)
        self.scope.select(self.deck.group_order.value)

    def update_subordinate_panels(self) -> None:
        self._update_face(self.latest)
        self._update_scope()

    def is_saved(self) -> bool:
        """is the current file saved?"""
        return self.filename[0] != '*'

    def is_main(self) -> bool:
        return self.focus == Focus.MAIN

    # PRIVATE METHODS

    def _save(self, command: str, deck_file: Optional[str] = None) -> None:
        if command == 'save' and deck_file is not None:
            self.filename = deck_file
        elif self.filename[0] == '*':
            self.filename = self.filename[1:]

        try:
            with open(self.filename, 'w') as f:
                f.write(self._export_all())
        except (FileNotFoundError, PermissionError) as e:
            raise DeckError(str(e))

        logger.info('deck saved to "{}"'.format(self.filename))
        if command == 'wq':
            raise ExitProgramException

    def _export_all(self, fronts_only: bool = False) -> str:
        sb = self._export_sideboard(fronts_only)
        if sb:
            return self._export_deck(fronts_only) + '\n\n' + sb
        return self._export_deck(fronts_only)

    def _export_deck(self, fronts_only: bool) -> str:
        return '\n'.join(['Deck'] + self._card_list(self.maindeck, fronts_only))

    def _export_sideboard(self, fronts_only: bool) -> str:
        cards = self._card_list(self.sideboard, fronts_only, sideboard = True)
        return '\n'.join(['Sideboard'] + cards) if cards else ''

    def _card_list(self, cards: Sequence[ModelCard],
                   fronts_only: bool, sideboard: bool = False) -> List[str]:
        names: List[str] = []
        for card in cards:
            name = card.back.name if fronts_only and card.is_back() else card.name
            count = card.sb_count if sideboard else card.md_count
            names.append('{} {}'.format(count, name))
        return names

    def _import_file(self, filename: str) -> Tuple[List[ModelCard], List[ModelCard]]:
        try:
            with open(filename, 'r') as f:
                deck, side = self._import_deck(f.read())
        except (FileNotFoundError, PermissionError) as e:
            raise DeckError(str(e))
        return deck, side

    def _import_deck(self, contents: str) -> Tuple[List[ModelCard], List[ModelCard]]:
        lines = list(enumerate(x.strip() for x in contents.splitlines()))
        logger.info('importing Deck...')
        deck: List[ModelCard] = []
        for i, line in lines:
            if line in ('', 'Deck'):
                continue
            if line == 'Sideboard':
                break
            try:
                logger.info('importing "{}"'.format(line))
                deck.extend(self._import_deck_line(line))
            except (ValueError, IndexError):
                raise DeckError('error parsing line #{}'.format(i + 1))
        else:
            return deck, []

        logger.info('importing Sideboard...')
        side: List[ModelCard] = []
        for j, line in lines[i + 1:]:
            if not line:
                continue
            try:
                logger.info('importing "{}"'.format(line))
                side.extend(self._import_deck_line(line))
            except (ValueError, IndexError):
                raise DeckError('error parsing line #{}'.format(i + j + 1))
        return deck, side

    def _import_deck_line(self, line: str) -> List[ModelCard]:
        """Handle various export formats

        From https://www.magicarenadeckbuilder.com/deckbuilder/importdeck?type=Arena :

        Arena's Format is: count card (SET) collector number, seperate side
        board with an empty line.

        Import should also work if you do not incude (SET) and collector
        number but may not find the exact card, do not incude collector number
        if you only add (SET). If you are importing a Magic Duels deck you do
        not (SET) or collector number.

        Examples:
        2 River Sneak (XLN) 70
        1 Chart a Course (XLN) 48
        1 Deeproot Waters (XLN) 51

        2 Glint-Nest Crane (KLD) 50
        1 Glassblower's Puzzleknot (KLD) 217

        Or
        1 River Sneak (XLN)

        Or (Magic Duels)
        1 River Sneak
        x1 River Sneak
        1x River Sneak
        """

        words = line.split()
        if '(' in words[-1]:
            name = ' '.join(words[1:-1])
        elif '(' in words[-2]:
            name = ' '.join(words[1:-2])
        else:
            name = ' '.join(words[1:])
        try:
            card = self._fetch_card_by_name(name)
        except (AttributeError, TypeError):
            raise DeckError('unknown card: "{}"'.format(name))
        count = int(words[0].replace('x', ''))
        return [card] * count

    def _fetch_card_by_name(self, name: str) -> ModelCard:
        query = "(name = '{}')".format(name.replace("'", "''"))
        card = ModelCard(self.db.fetchone(query))
        if card.is_flip_card():
            query = "(scryfall_id == '{}' AND is_back = {})".format(*card.back_key())
            card.back = ModelCard(self.db.fetchone(query), card)
        return card

    def _replace_deck(self, new_deck: List[ModelCard], new_sb: List[ModelCard],
                      filename: Optional[str] = None) -> None:
        # erase our old deck
        for key in [key for key in self.cards]:
            card = self.cards.pop(key)
            if self.results is not None:
                self.results.set_card_count(card, 0)
        self.deck = SortedDeck(self.deck.group_order)

        # add new cards to the maindeck
        for card in new_deck:
            count = self.add(card)
            if self.results is not None:
                self.results.set_card_count(card, count)

        # add new cards to the sideboard
        for card in new_sb:
            count = self.add(card)
            self.cards[card.id].sideboard(1)
            if self.results is not None:
                self.results.set_card_count(card, count)
        if filename is not None:
            self.filename = filename

        # and finally position us at the first card
        self.idx = 0 if len(self.cards) else None
        self.latest = self.current
        self._update_face(self.latest)
        self.view.update(self._display_deck(), self.idx, self._display_sb())

    def _remove_card(self, card: ModelCard) -> None:
        card = self.cards.pop(card.id)
        self.deck.remove(card)

    def _display_deck(self) -> Sequence[DisplayListing]:
        blank = DisplayListing()
        blank.left['blank'] = ''
        deck: List[DisplayListing] = []
        for section in self.deck.sections():
            cards = self.deck.cards(section)
            if cards:
                deck += self._display_cards(section, cards)
                deck.append(blank)
        return deck[:-1]

    def _display_cards(self, secname: str, cards: Sequence[ModelCard]) -> Sequence[DisplayListing]:
        if not cards:
            return []
        section = DisplayListing()
        if self.deck.group_order == Group.TYPES:
            secname = '{} {}'.format(sum(x.md_count for x in cards), secname)
        section.left['section'] = secname
        return [section] + [self._display_card(x) for x in cards]

    def _display_card(self, card: ModelCard, sb: bool = False) -> DisplayListing:
        result = DisplayListing()
        result.left['count'] = str(card.sb_count if sb else card.md_count)
        result.left['name'] = card.display_name()
        result.right['mana'] = card.mana
        return result

    def _display_sb(self) -> Sequence[DisplayListing]:
        blank = DisplayListing()
        blank.left['blank'] = ''
        title = DisplayListing()
        title.left['title'] = 'Sideboard ({}/15)'.format(len(self.deck.sideboard))
        return [blank, title] + [self._display_card(x, sb=True) for x in self.deck.sideboard]

    def _find_latest_index(self) -> Optional[int]:
        if self.latest is None or self.latest.id not in self.cards:
            return None
        try:
            return self.deck.index(self.latest)
        except ValueError:
            return None

    def _update_face(self, card: Optional[ModelCard]) -> None:
        self.is_flipped = False
        if card is None:
            self.face.update()
        elif card.is_modal_dfc():
            self.face.update(card.card, card.back.card)
        else:
            self.face.update(card.card)

    def _update_scope(self) -> None:
        self.scope.title = 'GROUP BY'
        listings: List[DisplayListing] = []
        for i, string in enumerate(['Mana Value (CMC)', 'Types']):
            dl = DisplayListing()
            dl.left['cmd'] = str(i + 1)
            dl.left['group_by'] = string
            listings.append(dl)
        self.scope.fill(listings, self.deck.group_order.value)

    def _log_card_change(self, func: str, card: ModelCard, count: int) -> None:
        logger.info('{}: {} "{}" card(s) in deck'.format(func, count, self.cards[card.id].name))

    def _mark_deck_as_modified(self) -> None:
        if self.filename[0] != '*':
            self.filename = '*' + self.filename
        else:
            self._update_title()

    def _untitled_deck_name(self) -> str:
        # return the next available untitled deck name
        name = self.DEFAULT_BASENAME + '.dec'
        if os.path.exists(name):
            i = 2
            while True:
                name = '{}{}.dec'.format(self.DEFAULT_BASENAME, i)
                if not os.path.exists(name):
                    break
                i += 1
        return name

    def _update_title(self) -> None:
        self.view.title = '{} ({}/60)'.format(self.filename, len(self.deck))
