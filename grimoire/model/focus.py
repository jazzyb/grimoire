from enum import auto, Enum


class Focus(Enum):
    """to keep track of whether we are in the maindeck or sideboard"""
    MAIN = auto()
    SIDE = auto()
