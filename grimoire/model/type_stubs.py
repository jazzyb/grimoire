# We have to have a circular dependency of imports between ResultModel and
# DeckModel for mypy, but the python interpreter doesn't like it.  Hence this
# file.
from typing import Any, TYPE_CHECKING
if TYPE_CHECKING:
    from .result_model import ResultModel as ResultModel
    from .deck_model import DeckModel as DeckModel
else:
    ResultModel = DeckModel = Any
