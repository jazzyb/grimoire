from collections import OrderedDict
from enum import Enum
from .model_card import ModelCard
from typing import Dict, Generator, List, Optional, Sequence

# FIXME:  This third-party library does not yet implement the correct type
# checking.  Until they do, we must ignore this type.
# see: https://github.com/grantjenks/python-sortedcontainers/pull/107
from sortedcontainers import SortedList  # type: ignore


class Group(Enum):
    CMC = 1
    TYPES = 2


class _Sideboard(object):
    def __init__(self) -> None:
        self.cards: SortedList[ModelCard] = SortedList(key = _key_by_cmc)

    def __getitem__(self, idx: int) -> ModelCard:
        return self.sideboard(self.cards)[idx]

    def __iter__(self) -> Generator[ModelCard, None, None]:
        yield from self.sideboard(self.cards)

    def __len__(self) -> int:
        return sum(x.sb_count for x in self.cards)

    def add(self, card: ModelCard) -> None:
        self.cards.add(card)

    def remove(self, card: ModelCard) -> None:
        _remove_from_list(card, self.cards)

    def sideboard(self, cards: Sequence[ModelCard]) -> Sequence[ModelCard]:
        return [x for x in cards if x.sb_count]


class SortedDeck(object):
    CMC = [str(i) for i in range(7)] + ['7+', 'Lands']
    TYPES = ['Planeswalkers',
             'Creatures',
             'Sorceries',
             'Instants',
             'Enchantments',
             'Artifacts',
             'Lands']

    def __init__(self, group_order: Group = Group.CMC) -> None:
        self.group_order = group_order
        self.deck: Dict[Group, OrderedDict[str, SortedList[ModelCard]]] = {}
        for group in Group:
            self.deck[group] = OrderedDict()
            for key in getattr(self, group.name)[:-1]:
                self.deck[group][key] = SortedList(key = _key_by_cmc)
            self.deck[group]['Lands'] = SortedList(key = _key_lands)
        self.sideboard = _Sideboard()

    def __getitem__(self, idx: int) -> ModelCard:
        count = 0
        for cards in self.deck[self.group_order].values():
            cards = self.mainboard(cards)
            if idx < len(cards) + count:
                card: ModelCard = cards[idx - count]
                return card
            count += len(cards)
        raise IndexError()

    def __len__(self) -> int:
        total = 0
        for cards in self.deck[Group.TYPES].values():
            total += sum(x.md_count for x in cards)
        return total

    @property
    def group_order(self) -> Group:
        return self._group_order

    @group_order.setter
    def group_order(self, new: Group) -> None:
        self._group_order = new

    def index(self, card: ModelCard) -> int:
        count = 0
        for cards in self.deck[self.group_order].values():
            cards = self.mainboard(cards)
            try:
                i: int = cards.index(card)
            except ValueError:
                count += len(cards)
            else:
                return i + count
        raise ValueError()

    def add(self, card: ModelCard) -> None:
        self.deck[Group.CMC][_cmc_key(card)].add(card)
        self.deck[Group.TYPES][_type_key(card)].add(card)
        self.sideboard.add(card)

    def remove(self, card: ModelCard) -> None:
        _remove_from_list(card, self.deck[Group.CMC][_cmc_key(card)])
        _remove_from_list(card, self.deck[Group.TYPES][_type_key(card)])
        self.sideboard.remove(card)

    def sections(self) -> Sequence[str]:
        return list(getattr(self, self.group_order.name))

    def cards(self, section: Optional[str] = None) -> Sequence[ModelCard]:
        if section is not None:
            return self.mainboard(self.deck[self.group_order][section])
        return sum([self.mainboard(cards) for cards in self.deck[self.group_order].values()], [])

    def mainboard(self, cards: Sequence[ModelCard]) -> Sequence[ModelCard]:
        return [x for x in cards if x.md_count]


# helper functions used by both SortedDeck and _Sideboard

def _cmc_key(card: ModelCard) -> str:
    """return the card's dict key for the CMC display"""
    if 'Land' in card.types:
        return 'Lands'
    if card.cmc > 6:
        return '7+'
    return str(card.cmc)


def _type_key(card: ModelCard) -> str:
    """return the card's dict key for the TYPES display"""
    if 'Planeswalker' in card.types:
        return 'Planeswalkers'
    if 'Creature' in card.types:
        return 'Creatures'
    if 'Sorcery' in card.types:
        return 'Sorceries'
    if 'Instant' in card.types:
        return 'Instants'
    if 'Enchantment' in card.types:
        return 'Enchantments'
    if 'Artifact' in card.types:
        return 'Artifacts'
    assert 'Land' in card.types
    return 'Lands'


def _key_lands(card: ModelCard) -> str:
    """sort lands by alpha with basics at the top"""
    if 'Basic' in card.types:
        return '0' + card.name
    return card.name


def _key_by_cmc(card: ModelCard) -> float:
    """sort cards by CMC first, then color (WUBRG order)"""
    key = 0
    if card.color_w:
        key = key * 10 + 1
    if card.color_u:
        key = key * 10 + 2
    if card.color_b:
        key = key * 10 + 3
    if card.color_r:
        key = key * 10 + 4
    if card.color_g:
        key = key * 10 + 5
    if key == 0:
        key = 20000
    return card.cmc + (key / 100000)


def _remove_from_list(card: ModelCard, sorted_list: List[ModelCard]) -> None:
    for i, item in enumerate(sorted_list):
        if card.id == item.id:
            del sorted_list[i]
            return
