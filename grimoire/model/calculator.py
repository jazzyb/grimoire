from ..error import GrimoireError
from functools import reduce
from math import factorial
import logging
logger = logging.getLogger(__name__)


def probability_mass_function(N: int, K: int, n: int, k: int) -> float:
    """Calculate the hypergeometric distribution

    Where:
        * N = population size (e.g. 60 card deck)
        * K = number of successes in the population (e.g. 24 lands in the deck)
        * n = sample size (e.g. 7 card opening hand)
        * k = number of successes in the sample (e.g. 2 lands in the hand)

    The probability of getting exactly k successes out of K random choices
    from a population of N items with a total of n successes is given by
    the formula:

        C(K, k) * C((N - K), (n - k)) / C(N, n)
    """
    logger.debug('Calc: -N {} -K {} -n {} -k {}'.format(N, K, n, k))
    if K < k or n < k:
        raise GrimoireError('error: calc: sample successes exceed population successes')
    numerator = binomial_coeff(K, k) * binomial_coeff(N - K, n - k)
    return numerator / binomial_coeff(N, n)


def binomial_coeff(n: int, r: int) -> float:
    """Calculate the number of combinations of size r in n

    Formula:
        n! / (r!(n - r)!)
    """
    return reduce(lambda x, y: x * y, [n - i for i in range(r)], 1) / factorial(r)


def at_least_prob(N: int, K: int, n: int, k: int) -> float:
    """Calculate the cumulative probability of getting *at least* k successes"""
    return sum(probability_mass_function(N, K, n, i) for i in range(k, min(K, n) + 1))
