from collections import namedtuple
from grimoire.data.card import CARD_TABLE
from grimoire.data.database import Database
from tempfile import NamedTemporaryFile as TempFile
import os
import unittest

def _get_temp_file():
    f = TempFile(delete=False)
    filename = f.name
    # We have to close the tempfile before returning the filename because
    # Windows won't let us have two open references to it at the same time for
    # whatever reason.
    f.close()
    return filename

Card = namedtuple('Card', [x[0] for x in CARD_TABLE])

def _create_default_card(name="Our Card Name"):
    data = [None] * len(CARD_TABLE)
    data[3] = name
    return Card(*data)

class TestDatabase(unittest.TestCase):
    def test_database(self):
        # test insert and fetch
        orig = _create_default_card()
        db1 = Database()
        db1.insert([orig])
        cards = db1.fetch('1=1')
        self.assertEqual(len(cards), 1)
        self.assertEqual(cards[0].name, orig.name)
        # test dump and load
        filename = _get_temp_file()
        db1.dump(filename)
        db1.close()
        db2 = Database(filename)
        os.unlink(filename)  # get rid of our tempfile
        cards = db2.fetch('1=1')
        self.assertEqual(len(cards), 1)
        self.assertEqual(cards[0].name, orig.name)
        db2.close()

    def test_fetch_order_by(self):
        card1 = _create_default_card()
        card2 = _create_default_card("Aaaaaaaaaa")
        db1 = Database()
        db1.insert([card1, card2])
        cards = db1.fetch('1=1', order_by = 'name')
        self.assertEqual(cards[0].name, card2.name)

if __name__ == '__main__':
    unittest.main()
