from grimoire.error import GrimoireError
from grimoire.search.lexer import Token
from grimoire.search.parser import Color, ManaCost, ManaValue, Name, parse, \
        PowerToughness, Rarity, SetId, UnknownOperatorError, UnknownSearchCommand
import unittest

def create_token(string):
    t = Token(string[0])
    for ch in string[1:]:
        if ch == '"':
            continue
        t.add(ch)
    return t

class TestParser(unittest.TestCase):
    def test_parsing_errors(self):
        token = create_token('r==m')
        with self.assertRaises(UnknownOperatorError):
            parse([token])

        token = create_token('r:')
        with self.assertRaises(GrimoireError):
            parse([token])

        token = create_token('win:true')
        with self.assertRaises(UnknownSearchCommand):
            parse([token])

    def test_name(self):
        token = create_token('I_AM_A_NAME')
        node = parse([token])[0]
        self.assertIsInstance(node, Name)
        self.assertEqual(str(node), "(name LIKE '%I_AM_A_NAME%')")

        token = create_token('-I_AM_NOT_A_NAME')
        node = parse([token])[0]
        self.assertIsInstance(node, Name)
        self.assertEqual(str(node), "(name NOT LIKE '%I_AM_NOT_A_NAME%')")

        token = create_token('!"SPECIFIC NAME"')
        node = parse([token])[0]
        self.assertIsInstance(node, Name)
        self.assertEqual(str(node), "(name IS 'SPECIFIC NAME')")

    def test_escape(self):
        token = create_token("Alrund's Epiphany")
        node = parse([token])[0]
        self.assertEqual(node.name, "Alrund''s Epiphany")

        token = create_token("o:\"Alrund's Epiphany\"")
        node = parse([token])[0]
        self.assertEqual(node.value, "Alrund''s Epiphany")

    def test_set(self):
        token = create_token('set:iko')
        node = parse([token])[0]
        self.assertIsInstance(node, SetId)
        self.assertEqual(str(node), "(set_id IS 'iko')")

        token = create_token('-set:m21')
        node = parse([token])[0]
        self.assertIsInstance(node, SetId)
        self.assertEqual(str(node), "(set_id IS NOT 'm21')")

        token = create_token('set>khm')
        with self.assertRaises(UnknownOperatorError):
            parse([token])

    def test_like_pattern(self):
        token = create_token('oracle:"whenever ~ enters the battlefield"')
        node = parse([token])[0]
        self.assertEqual(str(node), "(oracle LIKE '%whenever ~ enters the battlefield%')")

        token = create_token('-t:bird')
        node = parse([token])[0]
        self.assertEqual(str(node), "(types NOT LIKE '%bird%')")

        token = create_token('kw>haste')
        with self.assertRaises(UnknownOperatorError):
            parse([token])

    def test_rarity(self):
        token = create_token('rarity:rare')
        node = parse([token])[0]
        self.assertIsInstance(node, Rarity)
        self.assertEqual(str(node), "(rarity IN ('rare'))")

        token = create_token('r<=u')
        node = parse([token])[0]
        self.assertIsInstance(node, Rarity)
        self.assertEqual(str(node), "(rarity IN ('common', 'uncommon'))")

        token = create_token('-r>u')
        node = parse([token])[0]
        self.assertEqual(str(node), "(rarity NOT IN ('rare', 'mythic'))")

    def test_multicolor(self):
        token = create_token('color=m')
        node = parse([token])[0]
        self.assertIsInstance(node, Color)
        self.assertEqual(str(node), '(num_colors > 1)')

        token = create_token('-c>m')
        node = parse([token])[0]
        self.assertIsInstance(node, Color)
        self.assertEqual(str(node), '(num_colors <= 1)')

        token = create_token('color<m')
        node = parse([token])[0]
        self.assertEqual(str(node), '(num_colors <= 1)')

        token = create_token('-color<m')
        node = parse([token])[0]
        self.assertEqual(str(node), '(num_colors > 1)')

        token = create_token('color<=m')
        node = parse([token])[0]
        self.assertEqual(str(node), '(1)')

        token = create_token('-color<=m')
        node = parse([token])[0]
        self.assertEqual(str(node), '(0)')

        token = create_token('color>=3')
        node = parse([token])[0]
        self.assertEqual(str(node), '(num_colors >= 3)')

        token = create_token('c:wm')
        with self.assertRaises(GrimoireError):
            parse([token])

    def test_colorless(self):
        token = create_token('color:c')
        node = parse([token])[0]
        self.assertEqual(str(node), '(num_colors = 0)')

        token = create_token('-c<=c')
        node = parse([token])[0]
        self.assertEqual(str(node), '(num_colors > 0)')

        token = create_token('c:wc')
        with self.assertRaises(GrimoireError):
            parse([token])

    def test_colors(self):
        token = create_token('c>wu')
        node = parse([token])[0]
        # c>WU   =   (W and U) and (B or R or G)
        self.assertEqual(str(node), '((color_w AND color_u) AND (color_b OR color_r OR color_g))')

        token = create_token('c=wu')
        node = parse([token])[0]
        # c=WU   =   (W and U) and not (B or R or G)
        self.assertEqual(str(node), '((color_w AND color_u) AND NOT (color_b OR color_r OR color_g))')

        token = create_token('c<=bg')
        node = parse([token])[0]
        # c<=BG  =   not (W or U or R)
        self.assertEqual(str(node), '(NOT (color_w OR color_u OR color_r))')

        token = create_token('-c<=bg')
        node = parse([token])[0]
        self.assertEqual(str(node), 'NOT (NOT (color_w OR color_u OR color_r))')

        token = create_token('c<=wubrg')
        node = parse([token])[0]
        self.assertEqual(str(node), '(NOT (0))')

        token = create_token('c<wub')
        node = parse([token])[0]
        # c<WUB  =   not (W and U and B) and not (R or G)
        self.assertEqual(str(node), '(NOT (color_w AND color_u AND color_b) AND NOT (color_r OR color_g))')

        token = create_token('c>=brg')
        node = parse([token])[0]
        # c>=BRG =  (B and R and G)
        self.assertEqual(str(node), '(color_b AND color_r AND color_g)')

        token = create_token('c:brg')
        node = parse([token])[0]
        # c:BRG  =  same as >= above
        self.assertEqual(str(node), '(color_b AND color_r AND color_g)')

        token = create_token('c:x')
        with self.assertRaises(GrimoireError):
            parse([token])

    def test_cmc(self):
        token = create_token('cmc:3')
        node = parse([token])[0]
        self.assertIsInstance(node, ManaValue)
        self.assertEqual(str(node), '(cmc = 3)')

        token = create_token('cmc<10')
        node = parse([token])[0]
        self.assertEqual(str(node), '(cmc < 10)')

        token = create_token('-cmc<4')
        node = parse([token])[0]
        self.assertEqual(str(node), '(cmc >= 4)')

        token = create_token('cmc:x')
        with self.assertRaises(GrimoireError):
            parse([token])

    def test_power_toughness(self):
        prefix = "(power <> '' AND toughness <> '' AND "
        token = create_token('power:*')
        node = parse([token])[0]
        self.assertIsInstance(node, PowerToughness)
        self.assertEqual(str(node), prefix + "power = '*')")

        token = create_token('tou>pow')
        node = parse([token])[0]
        self.assertIsInstance(node, PowerToughness)
        self.assertEqual(str(node), prefix + "toughness > power)")

        token = create_token('toughness<3')
        node = parse([token])[0]
        self.assertEqual(str(node), prefix + "toughness < 3)")

        token = create_token('-tou<3')
        node = parse([token])[0]
        self.assertEqual(str(node), prefix + "toughness >= 3)")

    def test_loyalty(self):
        token = create_token('loy<3')
        node = parse([token])[0]
        self.assertEqual(str(node), "(loyalty <> '' AND loyalty < 3)")

    def test_mana_cost(self):
        token = create_token('mana=3rwr')
        node = parse([token])[0]
        self.assertIsInstance(node, ManaCost)
        self.assertEqual(str(node),
                "((mana LIKE '%{r}{r}%' AND mana LIKE '%{w}%' AND gmc = 3) AND (length(mana) = 12))")

        token = create_token('m:1rw')
        node = parse([token])[0]
        self.assertEqual(str(node),
                "(mana LIKE '%{r}%' AND mana LIKE '%{w}%' AND gmc >= 1)")

        token = create_token('-m>=wu')
        node = parse([token])[0]
        self.assertEqual(str(node),
                "NOT (mana LIKE '%{u}%' AND mana LIKE '%{w}%')")

        token = create_token('m>x{b/w}')
        node = parse([token])[0]
        self.assertEqual(str(node),
                "((mana LIKE '%{w/b}%' AND mana LIKE '%{x}%') AND (length(mana) > 8))")

        token = create_token('m>1wb')
        node = parse([token])[0]
        self.assertEqual(str(node),
                "((mana LIKE '%{b}%' AND mana LIKE '%{w}%') AND ((gmc > 1 AND length(mana) >= 9) OR (gmc = 1 AND length(mana) > 9)))")

        token = create_token('m<x')
        with self.assertRaises(GrimoireError):
            parse([token])

        token = create_token('m<=x')
        with self.assertRaises(GrimoireError):
            parse([token])

    def test_produces(self):
        token = create_token('p:wu')
        node = parse([token])[0]
        self.assertEqual(str(node), "(produces LIKE '%w%' AND produces LIKE '%u%')")

        token = create_token('-p:wu')
        node = parse([token])[0]
        self.assertEqual(str(node), "NOT (produces LIKE '%w%' AND produces LIKE '%u%')")

        token = create_token('p:m')
        node = parse([token])[0]
        self.assertEqual(str(node), "(LENGTH(REPLACE(produces, 'c', '')) > 1)")

        token = create_token('-p:m')
        node = parse([token])[0]
        self.assertEqual(str(node), "(LENGTH(REPLACE(produces, 'c', '')) <= 1)")

        token = create_token('p:wm')
        with self.assertRaises(GrimoireError):
            parse([token])

        token = create_token('p:x')
        with self.assertRaises(GrimoireError):
            parse([token])

if __name__ == '__main__':
    unittest.main()
