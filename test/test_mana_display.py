from grimoire.view.mana_display import ManaDisplay
from unittest.mock import call, MagicMock, patch
import unittest

COLORS = {'DEFAULT':   0,
          'WHITE':     1,
          'BLUE':      2,
          'BLACK':     3,
          'RED':       4,
          'GREEN':     5,
          'COLORLESS': 6}

def _mock_curses():
    mock_curses = MagicMock()
    mock_curses.color_pair = MagicMock(side_effect=list(range(7)))
    mock_curses.A_STANDOUT = 32
    mock_curses.A_BOLD = 16
    return mock_curses

class TestManaCost(unittest.TestCase):
    def test_insstr(self):
        curses = _mock_curses()
        with patch('grimoire.view.mana_display.curses', curses):
            mana = ManaDisplay('{X}{B/U}{G}')
            window = MagicMock()
            length = mana.insstr(window, 2, 5)
        self.assertEqual(length, 8)
        expected = [call.insstr(2, 5, 'X', COLORS['COLORLESS']),
                    call.insstr(2, 6, ' ', COLORS['COLORLESS']),
                    call.insstr(2, 7, 'B', COLORS['BLACK'] | curses.A_BOLD),
                    call.insstr(2, 8, '/', COLORS['DEFAULT']),
                    call.insstr(2, 9, 'U', COLORS['BLUE'] | curses.A_BOLD),
                    call.insstr(2, 10, ' ', COLORS['BLUE'] | curses.A_BOLD),
                    call.insstr(2, 11, 'G', COLORS['GREEN'] | curses.A_BOLD),
                    call.insstr(2, 12, ' ', COLORS['GREEN'] | curses.A_BOLD)]
        self.assertEqual(window.mock_calls, expected)

    def test_highlight(self):
        curses = _mock_curses()
        with patch('grimoire.view.mana_display.curses', curses):
            mana = ManaDisplay('{R}{W}')
            window = MagicMock()
            length = mana.insstr(window, 2, 9, highlight = True)
        self.assertEqual(length, 4)
        expected = [call.insstr(2, 9, 'R', COLORS['RED'] | curses.A_BOLD | curses.A_STANDOUT),
                    call.insstr(2, 10, ' ', COLORS['RED'] | curses.A_BOLD | curses.A_STANDOUT),
                    call.insstr(2, 11, 'W', COLORS['WHITE'] | curses.A_BOLD | curses.A_STANDOUT),
                    call.insstr(2, 12, ' ', COLORS['WHITE'] | curses.A_BOLD | curses.A_STANDOUT)]
        self.assertEqual(window.mock_calls, expected)

if __name__ == '__main__':
    unittest.main()
