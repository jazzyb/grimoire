from grimoire.search.lexer import tokenize
import unittest

class TestLexer(unittest.TestCase):
    def test_tokenize(self):
        tokens = tokenize('thassa legal:modern (-t=sorcery or pow>=tou) o:"whenever ~ enters the battlefield"')
        self.assertEqual(len(tokens), 8)

        self.assertFalse(tokens[0].negate)
        self.assertEqual(tokens[0].name, 'thassa')
        self.assertEqual(tokens[0].operator, '')
        self.assertEqual(tokens[0].value, '')

        self.assertEqual(tokens[1].name, 'legal')
        self.assertEqual(tokens[1].operator, ':')
        self.assertEqual(tokens[1].value, 'modern')

        self.assertEqual(tokens[2].name, '(')
        self.assertEqual(tokens[2].operator, '')
        self.assertEqual(tokens[2].value, '')

        self.assertTrue(tokens[3].negate)
        self.assertEqual(tokens[3].name, 't')
        self.assertEqual(tokens[3].operator, '=')
        self.assertEqual(tokens[3].value, 'sorcery')

        self.assertEqual(tokens[4].name, 'or')
        self.assertEqual(tokens[4].operator, '')
        self.assertEqual(tokens[4].value, '')

        self.assertEqual(tokens[5].name, 'pow')
        self.assertEqual(tokens[5].operator, '>=')
        self.assertEqual(tokens[5].value, 'tou')

        self.assertEqual(tokens[6].name, ')')
        self.assertEqual(tokens[6].operator, '')
        self.assertEqual(tokens[6].value, '')

        self.assertEqual(tokens[7].name, 'o')
        self.assertEqual(tokens[7].operator, ':')
        self.assertEqual(tokens[7].value, 'whenever ~ enters the battlefield')

    def test_syntax_error(self):
        with self.assertRaises(SyntaxError):
            tokenize('a>b<c')

if __name__ == '__main__':
    unittest.main()
