from grimoire.error import GrimoireError
from grimoire.model.calculator import at_least_prob, probability_mass_function
import unittest

class TestCalculator(unittest.TestCase):
    def test_hypergeo_calculator(self):
        prob = probability_mass_function(60, 24, 7, 2)
        self.assertTrue(0.269 < prob < 0.27)
        prob = at_least_prob(60, 24, 7, 2)
        self.assertTrue(0.857 < prob < 0.858)

    def test_sample_success_error(self):
        with self.assertRaises(GrimoireError):
            probability_mass_function(60, 10, 10, 11)
