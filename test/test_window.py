from grimoire.view.window import InterfaceWindow
from unittest.mock import call, Mock, patch
import unittest

class TestWindow(unittest.TestCase):
    @patch('grimoire.view.window.Panel', spec=True)
    @patch('grimoire.view.window.SearchBar', spec=True)
    @patch('grimoire.view.window.RunBar', spec=True)
    def test_window_panels(self, mock_run, mock_search, mock_panel):
        screen = Mock()
        mock_panel.return_value.win = screen
        mock_panel.return_value.height = 40
        mock_panel.return_value.width = 12
        InterfaceWindow.CARD_WIDTH = 55
        InterfaceWindow.CARD_HEIGHT = 30
        InterfaceWindow(screen, 51, 100)
        self.assertIn(call(screen, 30, 55, 1, 0), mock_panel.mock_calls)
        self.assertIn(call(screen, 19, 55, 31, 0), mock_panel.mock_calls)
        self.assertIn(call(screen, 49, 23, 1, 55), mock_panel.mock_calls)
        self.assertIn(call(screen, 49, 22, 1, 78), mock_panel.mock_calls)

    @patch('grimoire.view.window.Panel', spec=True)
    @patch('grimoire.view.window.SearchBar', spec=True)
    @patch('grimoire.view.window.RunBar', spec=True)
    def test_window_bars(self, mock_run, mock_search, mock_panel):
        screen = Mock()
        mock_panel.return_value.win = screen
        mock_panel.return_value.height = 40
        mock_panel.return_value.width = 12
        InterfaceWindow(screen, 50, 100)
        self.assertIn(call(screen, InterfaceWindow.SRCH_PROMPT, 0, 0, 100), mock_search.mock_calls)
        self.assertIn(call(screen, InterfaceWindow.EXEC_PROMPT, 49, 0, 50), mock_run.mock_calls)

if __name__ == '__main__':
    unittest.main()
