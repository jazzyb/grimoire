from grimoire.controller.run_bar import RunBar
from unittest.mock import call, MagicMock, patch
import curses.ascii
import os
import unittest

#import logging
#logging.basicConfig(level=logging.INFO)

# mock:
# * self.bar.edit(func)
#   * grab func to call manually in tests
# * self.bar.gather()
# * os.listdir()
# * os.path.isdir()

class TestRunBar(unittest.TestCase):
    @patch('grimoire.controller.entry_bar.curses')
    @patch('grimoire.controller.entry_bar.Textbox')
    def test_parse(self, bar, curses):
        runbar = RunBar(MagicMock(), 'test', 0, 0, 100)
        bar.return_value.edit = lambda _: "save"
        args = runbar.parse()
        self.assertEqual(args.command, "save")
        self.assertEqual(args.deck_file, None)

        bar.return_value.edit = lambda _: "save foobar.dec"
        args = runbar.parse()
        self.assertEqual(args.command, "save")
        self.assertEqual(args.deck_file, "foobar.dec")

        bar.return_value.edit = lambda _: "open foobar.dec"
        args = runbar.parse()
        self.assertEqual(args.command, "open")
        self.assertEqual(args.deck_file, ["foobar.dec"])

    @patch('grimoire.controller.entry_bar.curses')
    @patch('grimoire.controller.entry_bar.Textbox')
    def test_command_completion(self, bar, curses):
        runbar = RunBar(MagicMock(), 'test', 0, 0, 100)
        bar.return_value.gather = lambda: "q"
        bar.return_value.edit = self._edit_command_completion
        runbar.parse()
        runbar.entry.addstr.assert_called_with(0, 0, "quit ")

    def _edit_command_completion(self, validate):
        validate(curses.ascii.TAB)
        return "quit "

    @patch('grimoire.controller.entry_bar.curses.newwin')
    @patch('grimoire.controller.entry_bar.curses')
    @patch('grimoire.controller.entry_bar.Textbox')
    def test_command_completion(self, bar, curses, newwin):
        entry = MagicMock()
        entry.getyx = MagicMock()
        entry.getyx.return_value = (0, 0)
        newwin.return_value = entry
        self.runbar = RunBar(MagicMock(), 'test', 0, 0, 100)
        bar.return_value.gather = lambda: 'open ' + os.path.join('decks', 'ab')
        bar.return_value.edit = self._edit_filename_completion
        self.runbar.parse()

    def _edit_filename_completion(self, validate):
        with patch('grimoire.controller.run_bar.os.listdir', lambda _: ['abc', 'aaa', 'abd']):
            with patch('grimoire.controller.run_bar.os.path.isdir', lambda _: False):
                validate(curses.ascii.TAB)
                self.runbar.entry.addstr.assert_called_with(0, 0, 'open ' + os.path.join('decks', 'abc'))
                validate(curses.ascii.TAB)
                self.runbar.entry.addstr.assert_called_with(0, 0, 'open ' + os.path.join('decks', 'abd'))
                validate(curses.ascii.TAB)
                self.runbar.entry.addstr.assert_called_with(0, 0, 'open ' + os.path.join('decks', 'ab'))
        return "open abc"
