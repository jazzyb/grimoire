from grimoire.view.display_listing import DisplayListing
from grimoire.view.result_view import ResultView
from unittest.mock import call, MagicMock, patch
import unittest

def _mock_curses():
    mock_curses = MagicMock()
    mock_curses.color_pair.return_value = 0
    mock_curses.A_STANDOUT = 32
    mock_curses.A_BOLD = 16
    return mock_curses

class MockPanel(object):
    def __init__(self, mock):
        self.win = mock
        self.height = 5
        self.width = 20

class TestResultView(unittest.TestCase):
    def test_draw_data(self):
        window = MagicMock()
        panel = MockPanel(window)
        curses = _mock_curses()
        with patch('grimoire.view.base_view.curses', curses):
            v = ResultView(panel)
            v.start_idx = 2
            v.idx = 3
            v._draw_data(self._first_test_listing())
        self.assertEqual(list(window.mock_calls), self._first_expected_result())

    def test_draw_data_with_lines(self):
        window = MagicMock()
        panel = MockPanel(window)
        curses = _mock_curses()
        with patch('grimoire.view.base_view.curses', curses):
            v = ResultView(panel)
            v.start_idx = 2
            v.idx = 3
            v._draw_data(self._first_test_listing(), [2, 3])
        self.assertEqual(list(window.mock_calls), self._expected_result_with_lines())

    def test_update(self):
        window1 = MagicMock()
        panel = MockPanel(window1)
        curses = _mock_curses()
        with patch('grimoire.view.base_view.curses', curses):
            v = ResultView(panel)
            v.start_idx = 2
            v.idx = 3
            v._draw_data(self._first_test_listing())
            window2 = MagicMock()
            panel.win = window2
            v._window = window2
            v.update_data(self._second_test_listing())
        self.assertEqual(list(window2.mock_calls), self._second_expected_result())

    def test_move_up(self):
        win = MagicMock()
        panel = MockPanel(win)
        v = ResultView(panel)
        v._draw_data = MagicMock(name='_draw_data')
        v.idx = 0
        v.start_idx = 0
        v.move_up()
        self.assertFalse(v._draw_data.called)

        v.idx = v.start_idx = 6
        v.move_up()
        self.assertEqual(v.idx, 5)
        self.assertEqual(v.start_idx, 5)
        v._draw_data.assert_called_once_with([])

        v.start_idx = 0
        v.idx = 3
        v.move_up()
        self.assertEqual(v.idx, 2)
        v._draw_data.assert_called_with([], [4, 3])

    def test_move_down(self):
        win = MagicMock()
        panel = MockPanel(win)
        v = ResultView(panel)
        v._draw_data = MagicMock(name='_draw_data')
        listing = [0] * 6
        v._data = listing
        v.idx = 5
        v.move_down()
        self.assertFalse(v._draw_data.called)

        v.start_idx = 0
        v.idx = 3
        v._height = 5
        v.move_down()
        self.assertEqual(v.idx, 4)
        self.assertEqual(v.start_idx, 1)
        v._draw_data.assert_called_once_with(listing)

        v.start_idx = 0
        v.idx = 1
        v.move_down()
        self.assertEqual(v.idx, 2)
        v._draw_data.assert_called_with(listing, [2, 3])

    # PRIVATE METHODS

    def _first_test_listing(self):
        ret = []
        for i in range(10):
            dl = DisplayListing()
            dl.left['a'] = 'a' + str(i)
            dl.left['bold'] = 'b' + str(i)
            dl.left['c'] = 'cc' + str(i)
            dl.right['e'] = 'e' + str(i)
            dl.right['d'] = 'ddd' + str(i)
            ret.append(dl)
        return ret

    def _first_expected_result(self):
        return [call.insstr(0, 0, '===[ NO Results ]==='),
                call.erase(),
                call.insstr(0, 0, '===[ NO Results ]==='),
                call.insstr(1, 0, ' ' * 20, 0),
                call.insstr(1, 0, ' ', 0),
                call.insstr(1, 0, 'cc2', 0),
                call.insstr(1, 0, ' ', 0),
                call.insstr(1, 0, 'b2', 16),
                call.insstr(1, 0, ' ', 0),
                call.insstr(1, 0, 'a2', 0),
                call.insstr(1, 13, ' ', 0),
                call.insstr(1, 13, 'e2', 0),
                call.insstr(1, 13, ' ', 0),
                call.insstr(1, 13, 'ddd2', 0),
                call.insstr(2, 0, ' ' * 20, 32),
                call.insstr(2, 0, ' ', 32),
                call.insstr(2, 0, 'cc3', 32),
                call.insstr(2, 0, ' ', 32),
                call.insstr(2, 0, 'b3', 48),
                call.insstr(2, 0, ' ', 32),
                call.insstr(2, 0, 'a3', 32),
                call.insstr(2, 13, ' ', 32),
                call.insstr(2, 13, 'e3', 32),
                call.insstr(2, 13, ' ', 32),
                call.insstr(2, 13, 'ddd3', 32),
                call.insstr(3, 0, ' ' * 20, 0),
                call.insstr(3, 0, ' ', 0),
                call.insstr(3, 0, 'cc4', 0),
                call.insstr(3, 0, ' ', 0),
                call.insstr(3, 0, 'b4', 16),
                call.insstr(3, 0, ' ', 0),
                call.insstr(3, 0, 'a4', 0),
                call.insstr(3, 13, ' ', 0),
                call.insstr(3, 13, 'e4', 0),
                call.insstr(3, 13, ' ', 0),
                call.insstr(3, 13, 'ddd4', 0),
                call.insstr(4, 0, ' ' * 20, 0),
                call.insstr(4, 0, ' ', 0),
                call.insstr(4, 0, 'cc5', 0),
                call.insstr(4, 0, ' ', 0),
                call.insstr(4, 0, 'b5', 16),
                call.insstr(4, 0, ' ', 0),
                call.insstr(4, 0, 'a5', 0),
                call.insstr(4, 13, ' ', 0),
                call.insstr(4, 13, 'e5', 0),
                call.insstr(4, 13, ' ', 0),
                call.insstr(4, 13, 'ddd5', 0),
        ]

    def _expected_result_with_lines(self):
        return [call.insstr(0, 0, '===[ NO Results ]==='),
                call.erase(),
                call.insstr(0, 0, '===[ NO Results ]==='),
                call.insstr(2, 0, ' ' * 20, 32),
                call.insstr(2, 0, ' ', 32),
                call.insstr(2, 0, 'cc3', 32),
                call.insstr(2, 0, ' ', 32),
                call.insstr(2, 0, 'b3', 48),
                call.insstr(2, 0, ' ', 32),
                call.insstr(2, 0, 'a3', 32),
                call.insstr(2, 13, ' ', 32),
                call.insstr(2, 13, 'e3', 32),
                call.insstr(2, 13, ' ', 32),
                call.insstr(2, 13, 'ddd3', 32),
                call.insstr(3, 0, ' ' * 20, 0),
                call.insstr(3, 0, ' ', 0),
                call.insstr(3, 0, 'cc4', 0),
                call.insstr(3, 0, ' ', 0),
                call.insstr(3, 0, 'b4', 16),
                call.insstr(3, 0, ' ', 0),
                call.insstr(3, 0, 'a4', 0),
                call.insstr(3, 13, ' ', 0),
                call.insstr(3, 13, 'e4', 0),
                call.insstr(3, 13, ' ', 0),
                call.insstr(3, 13, 'ddd4', 0),
        ]

    def _second_test_listing(self):
        ret = []
        for i in range(2):
            dl = DisplayListing()
            dl.left['a'] = 'aaa' + str(i)
            ret.append(dl)
        return ret

    def _second_expected_result(self):
        return [
                call.insstr(1, 0, ' ' * 20, 32),
                call.insstr(1, 0, ' ', 32),
                call.insstr(1, 0, 'aaa0', 32),
                call.insstr(2, 0, ' ' * 20, 0),
                call.insstr(2, 0, ' ', 0),
                call.insstr(2, 0, 'aaa1', 0),
                call.insstr(3, 0, ' ' * 20, 0),
                call.insstr(4, 0, ' ' * 20, 0)
        ]

if __name__ == '__main__':
    unittest.main()
