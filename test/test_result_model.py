from grimoire.model.model_card import ModelCard
from grimoire.model.result_model import ResultModel
from typing import NamedTuple
from unittest.mock import call, MagicMock
import unittest

class MockCard(NamedTuple):
    name: str
    types: str
    mana: str
    cmc: int
    color_w: bool
    color_u: bool
    color_b: bool
    color_r: bool
    color_g: bool
    layout: str
    is_back: bool
    scryfall_id: str

_cards = [
        MockCard('aaa', '', '', 2, False, False, True, False, False, 'normal', False, '1'),
        MockCard('bbb', 'Land', '', 0, False, False, False, False, False, 'normal', False, '2'),
        MockCard('ccc', '', '', 1, False, True, False, False, False, 'normal', False, '3'),
]

_mdfcs = [
        MockCard('front', '', '', 1, True, False, False, False, False, 'modal_dfc', False, '4'),
        MockCard('back', '', '', 2, False, False, True, True, False, 'modal_dfc', True, '4'),
]

class TestResultModel(unittest.TestCase):
    def setUp(self):
        self.db = MagicMock()
        self.deck = MagicMock()
        self.view = MagicMock()
        self.face = MagicMock()
        self.scope = MagicMock()
        self.results = ResultModel(self.db, self.view, self.face, self.scope, self.deck)

    def test_update(self):
        self.db.fetch.return_value = _cards
        self.results.update('cardname')
        self.assertEqual(self.view.title, '3 Results')
        self.assertEqual(self.results.current.card, _cards[0])

    def test_move_up_down(self):
        self.db.fetch.return_value = _cards
        self.results.update('cardname')
        self.results.move_down()
        self.assertEqual(self.results.current.card, _cards[1])
        self.results.move_down()
        self.results.move_down()
        self.assertEqual(self.results.current.card, _cards[2])
        for _ in range(10):
            self.results.move_up()
        self.assertEqual(self.results.current.card, _cards[0])

    def test_goto_top_bottom(self):
        self.db.fetch.return_value = _cards
        self.results.update('cardname')
        self.results.goto_bottom()
        self.assertEqual(self.results.current.card, _cards[2])
        self.results.goto_top()
        self.assertEqual(self.results.current.card, _cards[0])

    def test_increment_normal(self):
        self.db.fetch.return_value = _cards
        self.results.update('cardname')
        self.deck.add.return_value = 1
        self.results.increment_current_card()
        self.deck.add.assert_called_once_with(self.results.current)
        self.view.replace.assert_called_once()
        self.view.temp_replace.assert_not_called()
        self.assertIn(_cards[0].scryfall_id, self.results.card_counts)

    def test_decrement_normal(self):
        self.db.fetch.return_value = _cards
        self.results.update('cardname')
        self.results.current.count = 2
        self.results.card_counts[self.results.current.card.scryfall_id] = 2
        self.deck.remove.return_value = 1
        self.results.decrement_current_card()
        self.deck.remove.assert_called_once_with(self.results.current)
        self.view.replace.assert_called_once()
        self.view.temp_replace.assert_not_called()
        self.assertIn(_cards[0].scryfall_id, self.results.card_counts)
        self.deck.remove.return_value = 0
        self.results.decrement_current_card()
        for count in self.results.card_counts.values():
            self.assertEqual(count, 0)

    def test_increment_mdfc(self):
        self.db.fetch.return_value = _mdfcs
        self.results.update('cardname')
        self.deck.add.return_value = 1
        self.results.increment_current_card()
        self.deck.add.assert_called_once_with(self.results.current)
        first = ModelCard(_mdfcs[1])
        first.count = 1
        second = ModelCard(_mdfcs[0])
        second.count = 1
        expected = [call.replace(self._display_card(first), None),
                    call.replace(self._display_card(second), 1)]
        self.assertEqual(self.view.mock_calls[-2:], expected)
        self.view.temp_replace.assert_not_called()
        self.results.is_flipped = True
        self.results.increment_current_card()
        self.assertEqual(self.deck.add.call_args[0][0].key(), first.back_key())
        self.view.temp_replace.assert_called_once_with(self._display_card(second))

    def test_order_by(self):
        self.db.fetch.return_value = _cards
        self.results.update('cardname')
        self.results.order_by(1)        # NAME
        self.view.assert_not_called()
        self.face.assert_not_called()
        self.scope.assert_not_called()
        self.results.order_by(2)        # COLOR
        self.assertEqual([x.name for x in self.results.cards], [_cards[2].name, _cards[0].name, _cards[1].name])
        self.results.order_by(3)        # CMC
        self.assertEqual([x.name for x in self.results.cards], [_cards[1].name, _cards[2].name, _cards[0].name])
        self.results.order_by(1)        # NAME
        self.assertEqual([x.name for x in self.results.cards], [x.name for x in _cards])

    def test_set_card_count(self):
        self.db.fetch.return_value = [_mdfcs[1]]
        self.db.fetchone.return_value = [_mdfcs[0]]
        self.results.update('cardname')
        card = ModelCard(_mdfcs[0], ModelCard(_mdfcs[1]), 100)
        self.results.set_card_count(card)
        self.assertEqual(self.results.card_counts[card.id], 100)
        self.assertEqual(self.view.replace.mock_calls,
                [call(self._display_card(card.invert()), 0)])

    def _display_card(self, mock_card):
        if type(mock_card) is not ModelCard:
            mock_card = ModelCard(mock_card)
        return self.results._display_card(mock_card)

if __name__ == '__main__':
    unittest.main()
