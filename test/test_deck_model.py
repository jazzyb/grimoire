from grimoire.model.model_card import ModelCard
from grimoire.model.deck_model import DeckError, DeckModel, Group
from typing import NamedTuple
from unittest.mock import MagicMock, patch
import unittest

class MockCard(NamedTuple):
    scryfall_id: str
    name: str
    mana: str
    types: str
    cmc: int
    color_w: bool
    color_u: bool
    color_b: bool
    color_r: bool
    color_g: bool
    is_back: bool
    layout: str

C1 = ModelCard(MockCard('1', 'Green', '', 'Creature', 5, False, False, False, False, True, False, 'normal'))
C2 = ModelCard(MockCard('2', 'Blue', '', 'Creature', 3, False, True, False, False, False, False, 'normal'))
C3 = ModelCard(MockCard('3', 'Azroius', '', 'Creature', 3, True, True, False, False, False, False, 'normal'))
C4 = ModelCard(MockCard('4', 'Colorless', '', 'Artifact Creature', 3, False, False, False, False, False, False, 'normal'))
C5 = ModelCard(MockCard('5', 'Abzan', '', 'Creature', 3, True, False, True, False, True, False, 'normal'))
C6 = ModelCard(MockCard('6', 'White', '', 'Creature', 5, True, False, False, False, False, False, 'normal'))
C7 = ModelCard(MockCard('7', 'Black', '', 'Creature', 8, False, False, True, False, False, False, 'normal'))
P = ModelCard(MockCard('11', 'Walker', '', 'Planeswalker', 4, True, True, False, False, False, False, 'normal'))
S = ModelCard(MockCard('12', 'Assassinate', '', 'Sorcery', 3, False, False, True, False, False, False, 'normal'))
I = ModelCard(MockCard('13', 'Shock', '', 'Instant', 1, False, False, False, True, False, False, 'normal'))
E = ModelCard(MockCard('14', 'Green Thing', '', 'Enchantment', 2, False, False, False, False, True, False, 'normal'))
A = ModelCard(MockCard('15', "Tormod's Crypt", '', 'Artifact', 0, False, False, False, False, False, False, 'normal'))
L1 = ModelCard(MockCard('16', 'Swamp', '', 'Basic Land', 0, False, False, False, False, False, False, 'normal'))
L2 = ModelCard(MockCard('17', 'A Land', '', 'Land', 0, False, False, False, False, False, False, 'normal'))
L3 = ModelCard(MockCard('18', 'B Land', '', 'Land', 0, False, False, False, False, False, False, 'normal'))

class TestDeckModel(unittest.TestCase):
    def setUp(self):
        self.db = MagicMock()
        self.view = MagicMock()
        self.face = MagicMock()
        self.scope = MagicMock()
        self.model = DeckModel(self.db, self.view, self.face, self.scope)
        self.deck = self.model.deck.deck

    def test_add(self):
        self.assertEqual(self.model.add(P), 1)
        self.assertEqual(self.model.add(P), 2)
        self.assertEqual(self.deck[Group.CMC][str(P.cmc)][0], P)
        self.assertEqual(self.deck[Group.TYPES]['Planeswalkers'][0], P)

        self.assertEqual(self.model.add(S), 1)
        self.assertEqual(self.deck[Group.CMC][str(S.cmc)][0], S)
        self.assertEqual(self.deck[Group.TYPES]['Sorceries'][0], S)
        self.assertEqual(self.model.add(I), 1)
        self.assertEqual(self.deck[Group.CMC][str(I.cmc)][0], I)
        self.assertEqual(self.deck[Group.TYPES]['Instants'][0], I)
        self.assertEqual(self.model.add(E), 1)
        self.assertEqual(self.deck[Group.CMC][str(E.cmc)][0], E)
        self.assertEqual(self.deck[Group.TYPES]['Enchantments'][0], E)
        self.assertEqual(self.model.add(L1), 1)
        self.assertEqual(self.deck[Group.CMC]['Lands'][0], L1)
        self.assertEqual(self.deck[Group.TYPES]['Lands'][0], L1)
        self.assertEqual(self.model.add(A), 1)
        self.assertEqual(self.deck[Group.CMC][str(A.cmc)][0], A)
        self.assertEqual(self.deck[Group.TYPES]['Artifacts'][0], A)
        self.assertEqual(self.model.add(C1), 1)
        self.assertEqual(self.deck[Group.CMC][str(C1.cmc)][0], C1)
        self.assertEqual(self.deck[Group.TYPES]['Creatures'][0], C1)

    def test_remove(self):
        self.assertEqual(self.model.remove(P), 0)
        self.model.add(P)
        self.model.add(P)
        self.assertEqual(self.model.remove(P), 1)
        self.assertEqual(self.model.remove(P), 0)
        self.assertEqual(len(self.deck[Group.CMC][str(P.cmc)]), 0)
        self.assertEqual(len(self.deck[Group.TYPES]['Planeswalkers']), 0)

    def test_sort_lands(self):
        self.assertEqual(self.model.add(L3), 1)
        self.assertEqual(self.model.add(L1), 1)
        self.assertEqual(self.model.add(L2), 1)
        self.assertEqual(self.deck[Group.CMC]['Lands'][0], L1)
        self.assertEqual(self.deck[Group.CMC]['Lands'][1], L2)
        self.assertEqual(self.deck[Group.CMC]['Lands'][2], L3)
        self.assertEqual(self.deck[Group.TYPES]['Lands'][0], L1)
        self.assertEqual(self.deck[Group.TYPES]['Lands'][1], L2)
        self.assertEqual(self.deck[Group.TYPES]['Lands'][2], L3)

    def test_7_plus_cmc(self):
        self.assertEqual(self.model.add(C7), 1)
        self.assertEqual(self.deck[Group.CMC]['7+'][0], C7)

    def test_sort_types(self):
        self.assertEqual(self.model.add(C1), 1)
        self.assertEqual(self.model.add(C2), 1)
        self.assertEqual(self.model.add(C3), 1)
        self.assertEqual(self.model.add(C4), 1)
        self.assertEqual(self.model.add(C5), 1)
        self.assertEqual(self.model.add(C6), 1)
        self.assertEqual(self.model.add(C7), 1)
        self.assertEqual(self.deck[Group.TYPES]['Creatures'][0], C2)
        self.assertEqual(self.deck[Group.TYPES]['Creatures'][1], C3)
        self.assertEqual(self.deck[Group.TYPES]['Creatures'][2], C5)
        self.assertEqual(self.deck[Group.TYPES]['Creatures'][3], C4)
        self.assertEqual(self.deck[Group.TYPES]['Creatures'][4], C6)
        self.assertEqual(self.deck[Group.TYPES]['Creatures'][5], C1)
        self.assertEqual(self.deck[Group.TYPES]['Creatures'][6], C7)

    def test_sort_cmc(self):
        self.assertEqual(self.model.add(C5), 1)
        self.assertEqual(self.model.add(C4), 1)
        self.assertEqual(self.model.add(C3), 1)
        self.assertEqual(self.model.add(C2), 1)
        self.assertEqual(self.model.add(S), 1)
        self.assertEqual(self.deck[Group.CMC]['3'][0], C2)
        self.assertEqual(self.deck[Group.CMC]['3'][1], S)
        self.assertEqual(self.deck[Group.CMC]['3'][2], C3)
        self.assertEqual(self.deck[Group.CMC]['3'][3], C5)
        self.assertEqual(self.deck[Group.CMC]['3'][4], C4)

    def test_save(self):
        self.model.add(C1)
        self.model.add(C2)
        self.model.add(C2)
        self.model.add(C7)
        self.model.add(C7)
        self.model.add(C7)
        args = MagicMock()
        args.command = 'save'
        with patch('grimoire.model.deck_model.open') as open_mock:
            open_mock.__enter__ = MagicMock()
            open_mock.__exit__ = MagicMock(return_value = False)
            self.model.process(args)
            open_mock().__enter__().write.assert_called_with('Deck\n1 Green\n2 Blue\n3 Black')

    def test_save_with_sideboard(self):
        self.model.add(C1)
        C1.sideboard(1)
        self.model.add(C2)
        self.model.add(C2)
        self.model.add(C7)
        self.model.add(C7)
        self.model.add(C7)
        args = MagicMock()
        args.command = 'save'
        with patch('grimoire.model.deck_model.open') as open_mock:
            open_mock.__enter__ = MagicMock()
            open_mock.__exit__ = MagicMock(return_value = False)
            self.model.process(args)
            open_mock().__enter__().write.assert_called_with('Deck\n2 Blue\n3 Black\n\nSideboard\n1 Green')

    def test_save_error(self):
        args = MagicMock()
        args.command = 'save'
        with patch('grimoire.model.deck_model.open') as open_mock:
            open_mock.side_effect = PermissionError('cannot write')
            with self.assertRaises(DeckError) as e:
                self.model.process(args)
            self.assertEqual(e.exception.args[0], 'cannot write')

    def test_open(self):
        self.db.fetchone = MagicMock(side_effect = [C1.card, C2.card, C7.card])
        args = MagicMock()
        args.command = 'open'
        args.deck_file = ['oops.dec']
        with patch('grimoire.model.deck_model.open') as open_mock:
            mock_file = MagicMock()
            mock_file.read = MagicMock(return_value = 'Deck\n1 Green (SET)\n2x Blue\n3 Black (SET) 365')
            open_mock.return_value.__enter__ = MagicMock(return_value = mock_file)
            open_mock.__exit__ = MagicMock(return_value = False)
            self.model.process(args)
        self.assertEqual(self.model.filename, 'oops.dec')
        self.assertIn(C1.id, self.model.cards)
        self.assertEqual(self.model.cards[C1.id].count, 1)
        self.assertIn(C2.id, self.model.cards)
        self.assertEqual(self.model.cards[C2.id].count, 2)
        self.assertIn(C7.id, self.model.cards)
        self.assertEqual(self.model.cards[C7.id].count, 3)
        self.assertEqual(len(self.model.cards), 3)

    def test_open_with_sideboard(self):
        self.db.fetchone = MagicMock(side_effect = [C2.card, C7.card, C1.card, C7.card])
        args = MagicMock()
        args.command = 'open'
        args.deck_file = ['oops.dec']
        with patch('grimoire.model.deck_model.open') as open_mock:
            mock_file = MagicMock()
            mock_file.read = MagicMock(return_value = 'Deck\n2 Blue\n3 Black\n\nSideboard\n1 Green\n1 Black')
            open_mock.return_value.__enter__ = MagicMock(return_value = mock_file)
            open_mock.__exit__ = MagicMock(return_value = False)
            self.model.process(args)
        self.assertEqual(self.model.filename, 'oops.dec')
        self.assertIn(C1.id, self.model.cards)
        self.assertEqual(self.model.cards[C1.id].md_count, 0)
        self.assertEqual(self.model.cards[C1.id].sb_count, 1)
        self.assertIn(C2.id, self.model.cards)
        self.assertEqual(self.model.cards[C2.id].md_count, 2)
        self.assertEqual(self.model.cards[C2.id].sb_count, 0)
        self.assertIn(C7.id, self.model.cards)
        self.assertEqual(self.model.cards[C7.id].md_count, 3)
        self.assertEqual(self.model.cards[C7.id].sb_count, 1)
        self.assertEqual(len(self.model.cards), 3)

    def test_open_error(self):
        args = MagicMock()
        args.command = 'open'
        args.deck_file = ['oops.dec']
        with patch('grimoire.model.deck_model.open') as open_mock:
            open_mock.side_effect = FileNotFoundError('cannot find')
            with self.assertRaises(DeckError) as e:
                self.model.process(args)
            self.assertEqual(e.exception.args[0], 'cannot find')

    def test_import_error(self):
        args = MagicMock()
        args.command = 'import'
        args.save = False
        with patch('grimoire.model.deck_model.pyperclip') as mock_pc:
            mock_pc.paste = MagicMock(return_value = 'Deck\n1 foo\n2 bar\nZ baz')
            with self.assertRaises(DeckError) as e:
                self.model.process(args)
        self.assertEqual(e.exception.args[0], 'error parsing line #4')

    def test_import_name_not_found(self):
        self.db.fetchone = MagicMock(return_value = [])
        args = MagicMock()
        args.command = 'import'
        args.save = False
        with patch('grimoire.model.deck_model.pyperclip') as mock_pc:
            mock_pc.paste = MagicMock(return_value = 'Deck\n1 NOT_A_NAME')
            with self.assertRaises(DeckError) as e:
                self.model.process(args)
        self.assertEqual(e.exception.args[0], 'unknown card: "NOT_A_NAME"')

if __name__ == '__main__':
    unittest.main()
