from grimoire.view.base_view import BaseView
from unittest.mock import call, Mock
import unittest

class MockPanel(object):
    def __init__(self, mock):
        self.win = mock
        self.height = 100
        self.width = 13

class TestBaseView(unittest.TestCase):
    def test_title(self):
        mock = Mock()
        v = BaseView(MockPanel(mock), 'foobar')
        self.assertIn(call.insstr(0, 0, '=[ foobar ]=='), mock.mock_calls)

if __name__ == '__main__':
    unittest.main()
