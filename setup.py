from distutils.core import setup

setup(name='Grimoire',
      version='0.1',
      description='A command-line deck-builder for Magic: The Gathering',
      author='Jason M Barnes',
      author_email='json.barnes@gmail.com',
      url='https://gitlab.com/jazzyb/grimoire',
      packages=['grimoire'],
      install_requires = [
          'pyperclip >= 1.8',
          'requests >= 2.26',
          'sortedcontainers >= 2.4',
          'windows-curses >= 2.2 ; sys_platform == "win32"'
      ]
)
