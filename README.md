# Summary

Grimoire is a command-line deck-builder for Magic: The Gathering.

# Usage (Quick-Start)

**First**, install Grimoire:
```bash
$ git clone https://gitlab.com/jazzyb/grimoire.git
$ cd grimoire
$ python -m pip install .
```

**Next**, download all cards in Future Standard from [Scryfall](https://scryfall.com/docs/api/bulk-data#files).  For example:
```bash
$ python -m grimoire download --sets "ZNR,KHM,STX,AFR,MID"
```
This will create a SQLite database at `scryfall-latest.sqlite`

**Finally**, run `grimoire` using the database created above:
```bash
$ python -m grimoire
```

This will start the Grimoire TUI.
There are a number of keyboard shortcuts for navigating the interface, which are documented [here](grimoire/controller/README.md).
See [this additional documentation](grimoire/view/README.md) to understand the layout.
From the search bar, you may lookup cards from the database using a subset of the Scryfall query language.  The search language is documented [here](grimoire/search/README.md).

# Etymology

Paraphrasing [Wikipedia](https://en.wikipedia.org/wiki/Grimoire):

> A grimoire (also known as a "book of spells") is a textbook of Magic,
> typically including instructions on how to create magical objects like
> talismans and amulets, how to perform magical spells, charms and
> [divination](https://scryfall.com/card/m19/51/divination),
> and how to summon or invoke supernatural entities such as
> [angels](https://scryfall.com/search?q=t%3Aangel&unique=cards&as=grid&order=name),
> [spirits](https://scryfall.com/search?q=t%3Aspirit&unique=cards&as=grid&order=name),
> [gods](https://scryfall.com/search?q=t%3Agod&unique=cards&as=grid&order=name) and
> [demons](https://scryfall.com/search?q=t%3Ademon&unique=cards&as=grid&order=name).

# Development

1. Install dependencies:
   ```bash
   python -m pip install -r requirements.txt
   ```
2. Run tests:
   ```bash
   make test
   ```
3. Type-checking and PEP8 validation of `grimoire/`:
   ```bash
   make check
   ```

# Documentation

* [Grimoire Search Language](grimoire/search/README.md):  How to search the database
* [Grimoire Visual Interface](grimoire/view/README.md):  How the interface is visually laid out
* [Grimoire User Input](grimoire/controller/README.md):  How to navigate the interface
* [Grimoire Database Schema](grimoire/data/README.md):  How the database is organized
